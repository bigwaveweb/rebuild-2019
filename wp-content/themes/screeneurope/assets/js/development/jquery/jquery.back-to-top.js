
	$(document).ready(function () {
		$(window).scroll(function() {
			if ($(window).scrollTop() > 500) {
				$('.js-back-to-top:hidden').stop(true, true).fadeIn();
			} else {
			    $('.js-back-to-top').stop(true, true).fadeOut();
			}
		});
		$('.js-back-to-top').click(function (event) {
			event.preventDefault();
			$('html,body').animate({ scrollTop: 0 }, 'slow');
		});
	});
