
$(function(){

		var $page_index = 1;
		$('.more-video').click(function(e){
			e.preventDefault();
			var data = {
				action	: 'video_ajax',
				idx			:	$page_index
			};
			$.ajax({
				type: 'POST',
				url: '/wp-admin/admin-ajax.php',
				data: data,
				success: function (data) {
					$page_index++;
					if(typeof data.html !== 'undefined'){
						$('.load-more-container .posts').append(data.html)
						if(data.html == ""){
							$('.more-video').hide();
						}
					}else{
						$('.more-video').hide();
					}
				},
				error: function(errorThrown){
					console.error(errorThrown);
					$('.more-video').hide();
				}
			});
		});

		//we clicked download the brochure..
		$('#download_brochure').click(function(e){
			console.log('---> download brochure', $('form.wpcf7-form').serializeArray() );
			window.brochure_download_type = 'brochure';
			window.brochure_language_select = $('#language_select').val();
		});

		//we clicked download the white paper..
		$('#download_white_paper').click(function(e){
			console.log('---> download white paper', $('form.wpcf7-form').serializeArray() );
			window.brochure_download_type = 'white_paper';
			window.brochure_language_select = $('#language_select').val();
		});

		//the form was submitted and the mail is sent - download a pdf....
		$(document).on('mailsent.wpcf7', function (event)
		{
			if( $('.brochure-link').length )
			{

				$('.brochure-download-form .form-error').html();

				var error_test = '';
				var url = $('.brochure-link').val() + error_test;

				///switch to the white paper
				if(window.brochure_download_type == 'white_paper'){

					var url = $('.white-paper-link').val() + error_test;

				}else{

					var brochure_lang = window.brochure_language_select;

					console.log('brochure lang', brochure_lang);

					switch(brochure_lang)
					{
						case 'FR':
							url = $('.brochure-link-fr').val() + error_test;
						break;

						case 'DE':
							url = $('.brochure-link-de').val() + error_test;
						break;

						default:
							//url is already url!!!
							break;
					}
				}

				console.log('---> Download Type:', window.brochure_download_type, url);

				$.ajax({
					url: url,
					type:'HEAD',
					error: function(){
						var str = 'A problem occurred when processing your request - please contact ' + $('.contact-email').val() + ' to receive a copy of the ' + $('.page-title').val() +' brochure';
						$('.brochure-download-form .form-error').html('<div class="unable-to-download">'+str+'</div>');
					},
					success: function()
					{
						//--> todo we need to pass in a better filename and the file location of the pdf
						window.location.href = '/download-attachment.php?f=' + url;
						console.log('---> The Url to the PDF: ', url)
					}
				});

			}
		});
		//scroll to function
		var scrollToAnchor = function(aid){
			var aTag = $("#"+ aid);
			if((aTag).length){
				var oset = 220;
				if($(window).width() < 768){
					oset = 0;
				}
				$('html,body').animate({scrollTop: $(aTag).offset().top - 220}, 'slow');
			}
		}
		//orange menu scroll
		$('[data-scroll]').click(function(e){
			e.preventDefault();
			var full_url = this.href;
			var parts = full_url.split("#");
			var trgt = parts[1];
			scrollToAnchor(trgt);
		})
		//scroll to product from mini menu
		$('[data-productidx]').click(function(e){
			e.preventDefault();
			var trgt = $(this).data('productidx');
			scrollToAnchor('productidx_' + trgt);
		});
		//brochure form append title to string - not really a fan of this!
		if( $('[name="newsletter[]"]').eq(1).length ){
			var val = $('[name="newsletter[]"]').eq(1).val();
			var title = $('.page-title').val();
			$('[name="newsletter[]"]').eq(1).next('span').text( val + ' ' + title);
		}
		//add the product name to the hidden to be used in the forms / emails
		if( $('[name="product_name"]').length ){
			var val = $('[name="newsletter[]"]').eq(1).val();
			var title = $('.page-title').val();
			$('[name="product_name"]').val(title);
		}
		//load the testimonial slick slider
		if( $('.testimonial-slider').length ){
			$('.testimonial-slider').slick({});
		}


})
