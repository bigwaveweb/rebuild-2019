(function($){
	$(document).ready(function () {
		$(window).scroll(function (event) {
			var $classToAdd = 'navbar-small';
			var $targetElem = $('.js-shrink-nav-on-scroll');

			if( $('#primary').length ){

				var $triggerPos = $('#primary').position().top;
				var $currentPos = $(document).scrollTop();

				if($currentPos > $triggerPos) {
					$targetElem.addClass($classToAdd);
					if($('#subnavigation').length){
						if(!$('#subnavigation').hasClass('in')){
							$('#subnavigation').collapse("show")
						}
					}
				} else {
					$targetElem.removeClass($classToAdd);
					if($('#subnavigation').length){
						if($('#subnavigation').hasClass('in')){
							$('#subnavigation').collapse("hide")
						}
					}
				}

			}

		})

		// Make Bootstrap dropdown slide on open/close
		$('.dropdown').on('show.bs.dropdown', function (e) {
		  var $height = $('.dropdown-menu').outerHeight() + 8;
		  $(this).find('.dropdown-menu').first().stop(true, true).slideToggle(200);
		  $(this).find('.dropdown-menu li a').css("height", $height);
		});

		$('.dropdown').on('hide.bs.dropdown', function (e) {
		  $(this).find('.dropdown-menu').first().stop(true, true).slideUp(200);
		});

		$(window).on("load resize", toggleStickyNav);
		function toggleStickyNav() {
			if ($(window).width() < 768 ) {
				$('#masthead').removeClass('fixed-top');
			} else {
				$('#masthead').addClass('fixed-top');
			}
		}

		//toggle search drop down
		$('.search__wrapper').click(function(e){
			e.preventDefault();
			$('#search').collapse('toggle');
		})
		//toggle search icon
		$('#search').on('hide.bs.collapse', function (e) {
			$('.search__wrapper a .fa-search').show();
			$('.search__wrapper a .fa-times').hide();
		})
		//toggle times icon
		$('#search').on('show.bs.collapse', function (e) {
			$('.search__wrapper a .fa-search').hide();
			$('.search__wrapper a .fa-times').show();
		})

		//end
	});
}(jQuery));
