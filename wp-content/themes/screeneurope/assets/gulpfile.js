var promise     = require('es6-promise').polyfill();
var gulp 	    = require('gulp');
var concat 	    = require('gulp-concat');
var stylus 	    = require('gulp-stylus');
var prefix 	    = require('gulp-autoprefixer');
var uglify 	    = require('gulp-uglify');
var notify 	    = require("gulp-notify");
var plumber     = require('gulp-plumber');
var coffee      = require('gulp-coffee');
var browserSync = require('browser-sync').create();
var package     = require('./package.json')

gulp.task('css', function () {
    gulp.start(['css_default','css_minify']);
});

gulp.task('css_default', function () {

    var onError = function(err) {
        notify.onError({
            title:    "Gulp error in " + err.plugin,
            message: err.toString(),
            sound:    "Beep"
        })(err);
        this.emit('end');
    };

    return gulp.src('css/development/master.styl')
        .pipe(plumber({errorHandler: onError}))
        .pipe(stylus({
            compress: false
        }))
        .pipe (prefix({
            browsers: ['last 12 versions'],
            cascade: false
        }))
        .pipe(concat('styles.css'))
        .pipe(gulp.dest('css'))
        .pipe(browserSync.stream())
        ;
});

gulp.task('css_minify', function () {

    var onError = function(err) {
        notify.onError({
            title:    "Gulp error in " + err.plugin,
            message: err.toString(),
            sound:    "Beep"
        })(err);
        this.emit('end');
    };

    return gulp.src('css/development/master.styl')
        .pipe(plumber({errorHandler: onError}))
        .pipe(stylus({
            compress: true
        }))
        .pipe (prefix({
            browsers: ['last 12 versions'],
            cascade: false
        }))
        .pipe(concat('styles.min.css'))
        .pipe(gulp.dest('css'))
});

gulp.task('js', function () {
    gulp.start(['js_default','js_minify']);
});

gulp.task('js_default', function () {

    return gulp.src([
		    'js/development/jquery/*.js',
		    'js/development/vue/*.js'
    ])
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest('js'));
});

gulp.task('js_minify', function () {

    return gulp.src([
        'js/development/jquery/*.js',
        'js/development/vue/*.js'
    ])
        .pipe(uglify())
        .pipe(concat('scripts.min.js'))
        .pipe(gulp.dest('js'));
});

gulp.task('default', function () {
    gulp.watch('js/development/**/*.js', function () {
        gulp.start('js');
    });
    gulp.watch('css/development/**/*.styl', function () {
        gulp.start('css');
    });

    browserSync.init({
        proxy: "screeneurope.bigwave.media"
    });
    gulp.watch("*.php").on("change", browserSync.reload);
});
