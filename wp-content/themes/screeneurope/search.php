<?php
get_header();
global $wp_query;
$total_results = $wp_query->found_posts;
?>
<div class="wrap">
	<div class="container">
		<div class="row">
			<?php if( (bool) $total_results && 1==2){ echo '<div class="padd-top-div"></div>';}?>
			<div id="primary" class="content-area">
				<main id="main" class="site-main bg-white" role="main">
					<div class="col-lg-7 col-md-8 col-sm-10 col-xs-12">
						<section class="row">
							<div class="col-xs-12">
								<h1>Search Results</h1>
							</div>
						</section>
						<section class="row">
						<?php
						if( have_posts() ){
							echo '<div class="col-xs-12"><p class="results-found">Result(s) found ' . $total_results . '.</p></div>';
							while ( have_posts() ) : the_post();
								echo '<article class="search-result col-xs-12">';
									global $post;

									switch($post->post_type){

										case 'marketingdepartment':
											$i_customer_id = $post->ID;
											//customer name
											$s_customer_name = $post->post_title;
											//customer position
											$s_customer_position = get_field('customer_position', $i_customer_id);
											//customer company
											$s_customer_company = get_field('customer_company', $i_customer_id);
											//customer location
											$s_customer_location = get_field('customer_location', $i_customer_id);
											//focus
											$s_customer_focus = get_field('customer_focus', $i_customer_id);
											//success
											$s_customer_success = get_field('customer_success', $i_customer_id);

											$content = array(
												$s_customer_name,
												$s_customer_position,
												$s_customer_company,
												$s_customer_location,
												$s_customer_focus,
												$s_customer_success
											);
											$emptyRemoved = array_filter($content);
											$content = implode(', ', $emptyRemoved);

										default:
											$content = strip_tags(apply_filters('the_content',  $post->post_content));
									}
									echo '
									<div style="margin-bottom:15px" class="ng-scope">
										<p>
											<a href="'.get_the_permalink().'" style="text-decoration:underline">'.products_solutions::subWords( get_the_title(), 70).'</a><br>
											<a href="'.get_the_permalink().'" class="small text-muted">'.products_solutions::subText(products_solutions::removeHTTPS(get_the_permalink()), 100).'</a><br>
											'.products_solutions::subWords($content, 190).'
										</p>
									</div>
								';
								echo '</article>';
							endwhile;
							echo '<div class="col-xs-12 search-pagination-container">' . paginate_links( array() ) . '</div>';
						}else{
							echo '<div class="col-xs-12"><p class="results-found">No results found.</p></div>';
						}
						?>
						</section>
					</div>
					<aside class="col-sm-4 col-sm-offset-1">
						<?php
						if( !(bool)$total_results ){
							dynamic_sidebar( 'sidebar-news-1' );
							show_all_tags_by_parent();
						}
						?>
					</aside>
				</main>
			</div>
		</div>
	</div>
</div>
<?php get_footer();
