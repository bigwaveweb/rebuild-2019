<?php
global $wp_post_types;
get_header(); ?>
<div class="wrap" class="job-post">
	<div class="container">
		<div class="row">
			<div id="primary" class="content-area">
				<div class="col-xs-12 category-news__container">
					<div id="primary" class="content-area category-news">
						<main id="main" class="site-main" role="main">
							<?php
							if ( have_posts() ) : ?>
							<div class="col-md-12">
								<?php
								$glob_id = false;
								while ( have_posts() ) : the_post();
									get_template_part( 'template-parts/post/content', get_post_format() );
									$glob_id = get_the_ID();
								endwhile;
								?>
							</div>
							<?php endif; ?>

						</main>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
