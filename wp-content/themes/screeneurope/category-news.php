<?php
get_header(); ?>
<div class="wrap" id="archive">
	<div class="container">
		<div class="row">
			<?php if ( have_posts() ) : ?>
			<div class="col-md-12">
				<header class="page-header">
					<h1 class="title__news">News &amp; Articles</h1>
				</header>
			</div>
			<?php endif; ?>
			<div class="col-xs-12 category-news__container">
				<div id="primary" class="content-area category-news">
					<main id="main" class="site-main" role="main">
                    </div>
						<?php

						if ( have_posts() ) : ?>

						<div class="col-sm-7">

							<?php

							while ( have_posts() ) : the_post();

							$formatVideo = get_post_format() === "video";

								if ($formatVideo) :

									get_template_part('template-parts/post/content-video', get_post_format());

								else :

									get_template_part('template-parts/post/content-excerpt', get_post_format());

								endif;

							endwhile;


							echo '<div class="search-pagination-container">';

							echo get_the_posts_pagination( array(

								'prev_text' => '<span class="">' . __( 'Previous', 'screeneurope' ) . '</span>',

								'next_text' => '<span class="">' . __( 'Next', 'screeneurope' ) . '</span>',

								'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( false, 'screeneurope' ) . ' </span>',

							) );

							echo '</div>';


							else :

								get_template_part( 'template-parts/post/content', 'none' );

							endif;

							?>


						</div>
						<div class="col-sm-4 col-sm-offset-1">
							<?php
								dynamic_sidebar( 'sidebar-news-1' );
								show_all_tags_by_parent();
							?>
						</div>
					</main>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer();
