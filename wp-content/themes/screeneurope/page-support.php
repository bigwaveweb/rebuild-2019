<?php
$glob_id = null;
get_header(); ?>
<div class="support-page">
	<div class="container">
		<div class="row">
			<div id="primary" class="content-area">
				<main id="main" class="site-main bg-white" role="main">
                <div class="col-xs-12">
                    </div>
					<div class="col-xs-12">
                        <?php
                        $applications = array();
                        while (have_posts()) : the_post();
                            $glob_id = get_the_ID();
                            ?>
							<div class="row">
								<div class="col-xs-12">
									<h1 class="page-title"><?php echo sgt::non_translate(get_the_title()); ?></h1>
								</div>
								<div class="col-xs-12">
									<div class="row">
										<div class="col-xs-12 col-sm-6">
											<div class="body-copy">
												<?php echo sgt::non_translate(get_the_content()); ?>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6">
											<table class="table table-dark hidden-xs">
												<tr>
                                                    <?php                                             
                                                    if (ICL_LANGUAGE_CODE == 'de') { ?>
                                                        <th>European Help Desk</th>
                                                    <?php } elseif (ICL_LANGUAGE_CODE == 'fr') { ?>
                                                        <th>Centre d’assistance européen</th>
                                                    <?php  } else { ?>
                                                        <th>European Help Desk</th>
                                                    <?php  } ?>
													<th>&nbsp;</th>
												</tr>
												<?php if (have_rows('support_help_desk_response')): ?>
														<?php while (have_rows('support_help_desk_response')): the_row(); ?>
															<tr>
																<td><?php echo get_sub_field('col_1'); ?></td>
																<td><?php echo get_sub_field('col_2'); ?></td>
															</tr>
														<?php endwhile; ?>
												<?php endif; ?>
											</table>
											<table class="table table-dark visible-xs">
												<tr>
													<th>European Help Desk</th>
												</tr>
												<?php if (have_rows('support_help_desk_response')): ?>
													<?php while (have_rows('support_help_desk_response')): the_row(); ?>
														<tr>
															<td><?php echo get_sub_field('col_1'); ?> </td>
														</tr>
														<tr>
															<td><?php echo get_sub_field('col_2'); ?></td>
														</tr>
													<?php endwhile; ?>
												<?php endif; ?>
											</table>
										</div>
									</div>
								</div>
							</div>
						<?php
                        endwhile;
                        ?>
					</div>
				</main>
			</div>
		</div>
	</div>
	<section class="support-options">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-4">
					<?php
                    $support_image = get_field('support_support_options_image', $glob_id);
                    if ($support_image) {
                        echo '<img src="'.$support_image['url'].'" alt="Image of Support Staff" />';
                    }
                    ?>
				</div>
				<div class="col-sm-7 col-sm-offset-1">
					<h2>Support Options</h2>
					<?php
                    $rows = get_field('support_support_options', $glob_id);
                    if ($rows) {
                        echo '<table class="table table-dark">';
                        foreach ($rows as $row) {
                            echo '<tr><th>'.$row['options_col_1'].'</th><td>'.sgt::non_translate($row['options_col_2']).'</td></tr>';
                        }
                        echo '</table>';
                    }
                    ?>
				</div>
			</div>
		</div>
	</section>
	<?php
    /*
    <section class="support-stages">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <h3>Service Portal</h3>
                    <a href="http://81.145.236.93/xdaweb/" target="_blank"><img style="width:auto" src="/assets/img/SD_Logogif.GIF" /></a>
                </div>

                <div class="col-xs-12 col-sm-6">
                    <h2>Screen Service Support</h2>
                    <p>Screen equipment has a reputation for reliability that is second to none and by providing first class service, throughout the EMEA region, Screen ensures that it retains its market leader status with equipment that has the highest up-time and lowest maintenance costs tailored specifically to individual requirements.
                    For specific details on any aspect of Screen service, please contact us;</p>
                    <?php echo do_shortcode('[contact-form-7 id="1383" title="Support Form"]'); ?>
                </div>

            </div>
        </div>
    </section>
    */
    ?>
	<?php
    /*
    <section class="support-stages">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 hidden-xs">
                    <?php
                    $support_image = get_field('support_support_stages_image', $glob_id);
                    if($support_image){
                        echo '<img src="'.$support_image['url'].'" alt="Image of Support Staff" />';
                    }
                    ?>
                </div>
                <div class="col-xs-12 visible-xs">
                    <img src="/assets/img/support-circles-1.jpg" alt="Support Stages" class="img-responsive">
                </div>
                <div class="col-xs-12 visible-xs">
                    <img src="/assets/img/support-circles-2.jpg" alt="Support Stages" class="img-responsive">
                </div>
            </div>
            <div class="row">
                <?php
                $rows = get_field('support_support_stages', $glob_id);
                if($rows){
                    foreach($rows as $row){
                ?>
                    <div class="col-sm-4">
                        <h3><?php echo sgt::non_translate($row['title']);?></h3>
                        <h4><?php echo sgt::non_translate($row['subtitle']);?></h4>
                        <?php echo sgt::non_translate($row['text']);?>
                    </div>
                <?php
                    }
                }
                ?>
            </div>
            <hr>
        </div>
    </section>
    */
    ?>
	<section class="support-trust">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-5">
					<?php
                    $support_image = get_field('support_support_trust_image', $glob_id);
                    if ($support_image) {
                        echo '<img class="support-trust-image" src="'.$support_image['url'].'" alt="Image of Support Staff" />';
                    }
                    ?>
				</div>
				<div class="col-md-7 col-md-offset-1 col-sm-7 col-sm-offset-0">
					<?php echo sgt::non_translate(get_field('support_support_trust_text', $glob_id)); ?>
				</div>
				<div class="col-xs-12 visible-xs">
					<hr>
				</div>
			</div>
		</div>
	</section>
	<section class="support-options">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-7">
					<?php echo get_field('screen_service_support', $glob_id); ?>
					<br>
					<?php
                    /*
                    <h3>Screen Service Support</h3>
                    <p>Screen equipment has a reputation for reliability that is second to
                    none and by providing first class service, throughout the EMEA region,
                    Screen ensures that it retains its market leader status with equipment
                    that has the highest up-time and lowest maintenance costs tailored
                    specifically to individual requirements.</p>
                    <p>For specific details on any aspect of Screen service, please contact us;</p>
                    */
                    ?>
                    <?php 

                    if (ICL_LANGUAGE_CODE == 'de') {
                        echo do_shortcode('[contact-form-7 id="2285" title="Support Form"]');
                    
                    } elseif (ICL_LANGUAGE_CODE == 'fr') { 
                        echo do_shortcode('[contact-form-7 id="2388" title="Support Form French"]');

                    } else {
                        echo do_shortcode('[contact-form-7 id="1383" title="Support Form"]');
                    } ?>
				</div>

				<?php /*
                <div class="col-xs-12 col-sm-5">
                    <div class="service-portal-div">
                        <?php echo get_field('service_portal', $glob_id);?>
                        <?php
                        ?>
                        <a href="http://81.145.236.93/xdaweb/" target="_blank"><img class="support-portal-button" style="width:auto" src="/assets/img/SD_Logogif.GIF" /></a>
                    </div>
                </div>
                */ ?>

			</div>
		</div>
	</section>
</div>
<?php get_footer();
