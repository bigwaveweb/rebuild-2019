<?php
/**
 * Video Ajax Functions load more videos, when lock more clicked!
 * --
 * @package WordPress
 * @subpackage Screen_Europe
 * @since 1.0
 */
add_action( 'wp_ajax_video_ajax', 'video_ajax' );
add_action( 'wp_ajax_nopriv_video_ajax', 'video_ajax');
/**
 * Extract the youtube link / tag from the content string
 * --
 * @param  integer $post_id Id of the current post
 * @return String           The Youtube ID embeded in iframe
 */
function get_my_first_video_embed($post_id = 0)
{
	$content = apply_filters('the_content', get_post_field('post_content', $post_id));
  $search     = '~(?:https?://)?(?:www.)?(?:youtube.com|youtu.be)/(?:watch\?v=)?([^\s]+)~';
  preg_match($search , strip_tags($content), $match);

  if(isset($match[1])){
    return ( '<iframe width="560" height="281" src="https://www.youtube.com/embed/'.$match[1].'" frameborder="0" allowfullscreen></iframe>');
    die();
  }
  return '';
}
/**
 * Get the Videos Via Ajax wp-query
 * --
 * @return [type] [description]
 */
function video_ajax()
{
  header('Cache-Control: no-cache, must-revalidate');
  header('Expires: '.gmdate('D, d M Y H:i:s \G\M\T', time() + 3600));
  header('Content-type: application/json');
  //query the DB for video posts
  $posts_per_page = 6;
  $offset = (( isset($_POST['idx']) ? $_POST['idx'] : 0 ) * $posts_per_page) + -2;
  //query the videos
  $query = new WP_Query(array(
    'posts_per_page' => $posts_per_page,
    'offset'         => $offset,
    'tag'            => isset($_POST['tag']) ? $_POST['tag'] : 'video'
  ));
  $html = '';
  if( isset($query->posts) && !empty($query->posts))
  {
    foreach( $query->posts as $item )
    {
      $html.='
      <div class="col-sm-6">
        <article id="post-'.$item->ID.'" class="post-'.$item->ID.' post type-post status-publish format-video hentry category-uncategorized tag-video post_format-post-format-video">
      	 <div class="video__container">';
      			if ( is_sticky() && is_home() ) {
      				$html .= screeneurope_get_svg( array( 'icon' => 'thumb-tack' ) );
      			}

            $html.='
            <header class="entry-header">'.$item->post_date.'</div>
              <h2 class="entry-title ajax-video-header"><a href="'.esc_url(  get_permalink($item->ID) ) . '" rel="bookmark">'.products_solutions::subWords($item->post_title, 80).'</a></h2>
        		</header>';

            $html .= '<div class="responsive-iframe">' . get_my_first_video_embed($item->ID) . '</div>';

            $html .='<div class="entry-content">';
            if (!empty($item->post_excerpt)) :
              $html .= "<p>" . $post->post_excerpt . "</p>";
            else :
              $html .= '<p>'.truncate_string(strip_shortcodes($item->post_content)) . '... <a href="'. get_permalink($post->ID) . '">' . 'Read More &raquo;' . '</a></p>';
            endif;
          $html .='</div>';

          $tags = get_the_tags($item->ID);

          $html .='<div class="tags__container">';
            foreach($tags as $tag){
              $html .=  '<span class="label label-default"><a href="/tag/'.$tag->slug.'" rel="tag">'.$tag->name.'</a></span> ';
            }
            $html .= '</div>';

        $html .= '
        </article><!-- #post-## -->
        </div>
        ';
    }

    echo json_encode(array(
      'success' => 'ok',
      'post'    => $_POST,
      'query'   => $query,
      'html'    => $html
    ));

  }else{
    echo json_encode(array(
      'success' => 'fail',
      'message' => 'no posts'
    ));
  }

  wp_die();
}
