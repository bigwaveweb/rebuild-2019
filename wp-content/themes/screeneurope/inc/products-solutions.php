<?php
/*******************************************************************************
Additional Functions for Products / Solutions section
*******************************************************************************/


/**
 * Products and Solutions class - functionality for product & marketing pages
 * --
 */
class products_solutions
{
  /**
   * The main top level product cats.
   * --
   * @var [type]
   */
  public static $product_cats = array('label-packaging', 'high-speed-inkjet', 'wide-format', 'prepress');
  /**
   * Excluded from the marketng page
   * --
   * @var [type]
   */
  public static $excluded_marketing_product_cats = array('prepress');
  /**
  * Call this method to get singleton
  * --
  * @return UserFactory
  */
  public static function getInstance()
  {
    static $inst = null;
    if ($inst === null) {
      $inst = new products_solutions();
    }
    return $inst;
  }
  /**
  * Private ctor so nobody else can instantiate it
  *
  */
  private function __construct()
  {}
  /**
   * Get the product categories - top level
   * --
   * Todo: Get this from the DB
   * --
   * @return [type] [description]
   */
  public static function get_product_categories()
  {
    return self::$product_cats;
  }
  /**
   * Get the end slug from the url
   * --
   * @var [type]
   */
  public static function get_end_slug()
  {
    $parts = explode('/', rtrim($_SERVER['REQUEST_URI'], '/'));
    return end($parts);
  }
  /**
   * Get Product Testimonials from product id's
   * --
   * Optional Turn on random and return a specific amount.
   * --
   * @param  array  $product_ids [description]
   * @return [type]              [description]
   */
  public static function getParentProductTestimonials( $product_ids = array(), $random = false, $amount = 10 )
  {
    $testimonials = array();
    foreach( $product_ids as $current_product_id){
       $tmp = self::getProductTestimonials($current_product_id);
       foreach( $tmp as $item){
         $testimonials[] = $item;
       }
    }
    if($random){
      shuffle($testimonials);
      return array_slice($testimonials, 0, $amount);
    }
    return  $testimonials;
  }
  /**
   * Get products by tag name
   * --
   * @param  boolean $tag_name The tag slug
   * @return WP_Query            WP Query Object containing the products matched by tag
   */
  public static function get_products_by_tag($tag_name = FALSE)
  {
    if(!$tag_name){
      return array();
    }
    $args = array(
      'post_type' => 'products',
      'posts_per_page' => 99,
      'tag' => $tag_name
    );
    return new WP_Query( $args );
  }
  /**
   * Get Product Testimonials using product ID
   * --
   * @param  boolean $new_id [description]
   * @return [type]          [description]
   */
  public static function getProductTestimonials( $new_id = false )
  {
    $matched = array();
    $product_slug  = self::get_end_slug();

    if( $new_id ){
      $product = get_post($new_id);
    }else{
      $product = getProductBySlug($product_slug);
    }
    if($product && isset($product->ID))
    {
      //get the id of the current product - that will come in handy
      $id = $product->ID;
      //query the for all of the testimonials
      $args = array(
        'post_type'   => 'testimonial',
      );
      $testimonials = get_posts($args);
      //see if they have the same id as the product id
      foreach( $testimonials as $item)
      {
        $related_products  = get_field('testimonial_product', $item->ID);
        foreach($related_products as $row)
        {
          if($row->ID == $id){
            //we have matched!
            $matched[] = $item;
          }
        }
      }
    }
    return $matched;
  }
  /**
   * [getTestimonialHtml description]
   * --
   * @param  boolean $item [description]
   * @return [type]        [description]
   */
  public static function getTestimonialHtml( $item = Object )
  {
    if (!$item){
      return;
    }
    //the quote text
    $s_quote = $item->post_content;
    //the customer object
    $o_customer = get_field('testimonial_customer', $item->ID);
    if(is_array($o_customer) && isset($o_customer[0])){
      $o_customer = $o_customer[0];
    }
    //return if we do not have the customer - there should only be one customer!
    if(!is_object($o_customer)){
      return '';
    }
    //the customer id
    $i_customer_id = $o_customer->ID;
    //customer name
    $s_customer_name = $o_customer->post_title;
    //customer position
    $s_customer_position = get_field('customer_position', $i_customer_id);
    $s_customer_position = !empty($s_customer_position) ? ', ' . $s_customer_position : '';
    //customer company
    $s_customer_company = get_field('customer_company', $i_customer_id);
    //customer location
    $s_customer_location = get_field('customer_location', $i_customer_id);
    $s_customer_location = !empty($s_customer_location) ? ', ' . $s_customer_location : '';

    return '
    <blockquote class="col-xs-12">
      <div class="testimonial-padding">
        <div class="testimonial-content">
          <p class="testimonal">'.self::subWords($s_quote, 200).'</p>
        </div>
        <p class="by">'.$s_customer_name.$s_customer_position.'<br>'.$s_customer_company.$s_customer_location.'</p>
      </div>
    </blockquote>
    ';
  }
  /**
   * Ellispsis by words
   * --
   * @param  string  $string [description]
   * @param  integer $count  [description]
   * @return [type]          [description]
   */
  public static function subWords($string = "", $count = 100)
  {
    if(strlen($string) <= $count ){
      return $string;
    }
    $tmp = substr($string, 0, $count);
    $tmp = explode(' ', $tmp);
    array_pop($tmp);
    $tmp = implode(' ', $tmp);
    $tmp = $tmp . '...';
    return $tmp;
  }
  /**
   * Ellipsis text - no check for works, just substr the string.
   * --
   * @param  string  $string [description]
   * @param  integer $count  [description]
   * @return [type]          [description]
   */
  public static function subText($string = "", $count = 50)
  {
    if(strlen($string) <= $count ){
      return $string;
    }
    $tmp = substr($string, 0, $count);
    $tmp = $tmp . '...';
    return $tmp;
  }
  /**
   * Get Customers assigned to a product
   * --
   * @param  boolean $product_id [description]
   * @return [type]              [description]
   */
  public static function getProductCustomers( $product_id = false )
  {
    if(!$product_id){
      return false;
    }
    $customers = get_posts(array(
      'post_type' => 'marketingdepartment',
      'meta_query' => array(
        array(
          'key' => 'customer_products_purchased',
          'value' => '"' . $product_id . '"',
          'compare' => 'LIKE'
        )
      )
		));
    return $customers;
  }
  /**
   * Get all of the customers for a selection of products.
   * --
   * @param  array  $product_ids The selection of products
   * @return Array              collection of customers who brought the products.
   */
  public static function getMultipleProductCustomers( $product_ids = array(), $excluded_person = FALSE )
  {
    if(empty($product_ids) || !$product_ids) return array();
    $customers_used = array();
    if($excluded_person){
      $customers_used[] = $excluded_person;
    }
    $return_customers = array();
    foreach( $product_ids as $product_id ){
      $tmp_customers = self::getProductCustomers($product_id);
      foreach( $tmp_customers as $customer){
        if(isset($customer->ID) && !in_array($customer->ID, $customers_used) ){
          $return_customers[] = $customer;
        }
        $customers_used[] = $customer->ID;
      }
    }
    return $return_customers;
  }
  /**
   * [slugToTitle description]
   * --
   * @param  string $slug [description]
   * @return [type]       [description]
   */
  public static function slugToTitle($slug = ""){
    return ucwords(str_replace('-', ' ', $slug));
  }
  /**
   * Retrieve Quotes for a customer
   * --
   * @param  boolean $id_customer The Unique Identifier of the customer
   * @return Array        Array of wordpress post objects
   */
  public static function getQuotesForCustomer( $id_customer = FALSE)
  {
    if(is_null($id_customer) || FALSE === $id_customer || empty($id_customer) ){
      return '';
    }
    $testimonials = get_posts(array(
      'post_type' => 'testimonial',
      'meta_query' => array(
        array(
          'key' => 'testimonial_customer',
          'value' => '"' . $id_customer . '"',
          'compare' => 'LIKE'
        )
      )
  	));
    return $testimonials;
  }
  /**
   * Take a randon slice from an array
   * --
   * @param  array  $data Array of daya
   * @return [type]       Array Shuffled and sliced
   */
  public static function randomSlice($data = array(), $amount = 1)
  {
    shuffle($data);
    if(count($data) < $amount){
      return $data;
    }
    return array_slice($data, 0, $amount);
  }
  /**
   * Retrieve the product tagged to a quote
   * --
   * @param  boolean $quote_id [description]
   * @return [type]            [description]
   */
  public static function getProductByQuote($quote_id = FALSE)
  {
    if( is_null($quote_id) || FALSE ===($quote_id) || empty($quote_id)){
      return '';
    }
    $product = get_field( 'testimonial_product', $quote_id );
    if( is_array($product) && isset($product[0])){
      return $product[0];
    }
    return $product;
  }
  /**
   * Get all of the tags assigned to the post, return as array or a string
   * --
   * @param  boolean $id_post   The ID of the post to get tags for
   * @param  boolean $getString Should we return the results as CSV string or array
   * @return MIXED             Either CSV or array of results
   */
  public static function getAlltagsForPost( $id_post = FALSE, $getString = TRUE )
  {
    if( is_null($id_post) || FALSE ===($id_post) || empty($id_post) ){
      return '';
    }
    $tmp = array();
    $posttags = get_the_tags( (int)$id_post );
    if(!empty($posttags))
      foreach($posttags as $tag){
        $tmp[] = $tag->slug;
      }
    if(!$getString){
      return $tmp;
    }
    return implode(',', $tmp);
  }
  /**
   * Using a tag string get all related news posts
   * --
   * @param  boolean $tag_str The tag String, test for false or empty
   * @return Array           Return a random slice of the posts array (2x random)
   */
  public static function getStoriesByTag($tag_str = FALSE, $catname = "news", $limit = 2)
  {
    if( is_null($tag_str) || FALSE === $tag_str || empty($tag_str) ){
      return '';
    }
    $args = array(
      'tag'           => $tag_str,
      'post_type'     => 'post',
    );
    if($catname){
      $args['category_name'] = $catname;
    }

    $query = new WP_Query($args);
    if(isset($query->posts) && is_array($query->posts) && !empty($query->posts)){
      return self::randomSlice($query->posts, $limit);
    }
    return FALSE;
  }
  /**
   * Get the Format of the item, is it a video ?
   * --
   * @param  integer $id_post The Id of the post to compare
   * @return boolean          Return true if a video
   */
  public static function isVideoFormat( $id_post = 0)
  {
    $_format = get_the_terms( $id_post, 'post_format' );
    if( isset($_format[0]) && strstr($_format[0]->slug, 'video' )){
      return true;
    }
    return false;
  }
  /**
   * Get the Marketing Peoples
   * --
   * @return [type] [description]
   */
  public static function getTheMarketingPeeps( $excluded_person = FALSE )
  {
    //get the excluded list
    $excluded = self::$excluded_marketing_product_cats;

    //foreach $category
    foreach( self::get_product_categories() as $category)
    {
      //some appear to be excluded from this page.
      if(in_array($category, $excluded)){
        continue;
      }
      //get the products and save the id's
      $product_ids = array();
      $products = self::get_products_by_tag( $category );
      if ( $products->have_posts() ) :
        while ( $products->have_posts() ) : $products->the_post();
          //captya the product id
          $product_ids[] = get_the_ID();
        endwhile;
      endif;
      $_REQUEST['product_ids'] = $product_ids;

      if(!empty($_REQUEST['product_ids'])){
        echo '
        <div class="row">
          <div class="col-xs-12">
            <div class="row">
              <div class="col-xs-12">
                <div class="arrow-header small-blue">
                  <h2><b>'.self::slugToTitle($category).'</b></h2>
                </div>
              </div>
            </div>
          </div>
        </div>
        ';
        echo '<div class="row">';
          if($excluded_person){
            $_REQUEST['excluded_person'] = $excluded_person;
          }
          get_template_part( 'template-parts/products/product-customers' );
          unset($_REQUEST['product_ids']);
          unset($_REQUEST['excluded_person']);
        echo '</div>';
      }
    }
  }
  /**
   * Get Four tagged Support Videos
   * --
   * @return Array Collection of WP Post Objects
   */
  public static function getFourTaggedSolutions()
  {
    return self::getStoriesByTag('video+support', FALSE, 4);
  }
  /**
   * Get the four solutions that can be used on the homepage.
   * ===
   * @return [type] [description]
   */
  public static function getSolutions()
  {
    $query = new WP_Query( array( 'pagename' => 'solutions' ) );
    if(!isset($query->posts[0]->ID)){
      return array();
    }
    $args = array(
      'post_type' => 'page',
      'post_status' => 'publish',
      'posts_per_page' => -1,
      'post_parent' => $query->posts[0]->ID,
      'order' => 'ASC',
    );
    $pages = new WP_Query($args);
    if(isset($pages->posts)){
      return $pages->posts;
    }
    return array();
  }
  /**
   * Get the products for the homepage testimonials
   * --
   * @return [type] [description]
   */
  public static function getHomepageTestimonialProducts()
  {
    $ids = array();
    $args = array(
      'post_type' => 'products',
      'posts_per_page' => 99,
      'published'   => 1
    );
    $query = new WP_Query( $args );
    if(isset($query->posts)){
      foreach($query->posts as $item){
        $ids[] = $item->ID;
      }
    }
    return $ids;
  }
  /**
   * Get related Posts to provided id.
   * --
   * @param  integer $current_post_id [description]
   * @param  [type]  $post_type       [description]
   * @return [type]                   [description]
   */
  public static function get_related_posts($current_post_id = 0, $post_type = NULL)
  {
    $tags = wp_get_post_tags($current_post_id);
    $tmpTags = array();
    foreach($tags as $tag){
      $tmpTags[] = $tag->term_id;
    }
    $args = array(
      'tag__in'               => $tmpTags,
      'post__not_in'          => array($current_post_id),
      'posts_per_page'        => 5,
      'ignore_sticky_posts'   => 1
    );
    if( NULL !== $post_type ){
      $args['post_type'] = $post_type;
    }
    $results = new WP_Query($args);
    return $results;
  }
  /**
   * Get re;ated products html to be used in the blofgs sidebars
   * --
   * @param  integer $parent_id [description]
   * @return [type]             [description]
   */
  public static function getRelatedProductsHtml( $parent_id = 0, $post_count = NULL )
  {
    $related_products = self::get_related_posts($parent_id, 'products');
    if( $related_products->have_posts() ) {
      echo '<div class="related-videos">';

        $limit = 1;

        while ($related_products->have_posts()) : $related_products->the_post();

          if( !is_null($post_count) && ($limit > $post_count)  ){
            continue;
          }

          $allow_page = get_field('allow_page', get_the_ID() );

          echo '
          <div style="margin-bottom:25px">
            <h3 class="light" style="margin-bottom:0">
          ';

          if($allow_page){
              echo '<a href="'.get_the_permalink().'">'.get_the_title().'</a>';
          }else{
              echo '<a>'.get_the_title().'</a>';
          }

          echo'</h3>
          <p>'.get_the_title().'</p>
          <div class="row">
            <div class="col-xs-12">
              <p><a href="'.get_the_permalink().'"><img src="'.get_the_post_thumbnail_url( get_the_id() ).'" class="img-responsive" alt="Image of '.get_the_title().'"></a></p>
            </div>
          </div>
          ';

          if($allow_page){
            echo '<p><a href="'.get_the_permalink().'" class="btn btn-default">'.get_the_title().' &raquo;</a></p>';
          }else{
            $brochure = get_field( "brochure", get_the_id() );
          	$brochureLink = isset($brochure['url']) ? $brochure['url'] : false;
          	if($brochureLink){
              if(ICL_LANGUAGE_CODE == 'de') {
          		  echo '<p><a class="btn btn-default" href="'.$brochureLink.'"><span class="glyphicon glyphicon-download-alt"></span> Broschüre herunterladen (PDF)</a><p>';
              } elseif(ICL_LANGUAGE_CODE == 'fr') {
                echo '<p><a class="btn btn-default" href="'.$brochureLink.'"><span class="glyphicon glyphicon-download-alt"></span> Télécharger la brochure (PDF)</a><p>';
              } else {
                echo '<p><a class="btn btn-default" href="'.$brochureLink.'"><span class="glyphicon glyphicon-download-alt"></span> Download Brochure (PDF)</a><p>';
              }
            }
          }

          echo '
          </div>
          ';

          $limit ++;

        endwhile;
      echo '</div>';
    }
  }
  /**
   * Using the end slug find the product id
   * --
   * @return Integer The ID of the product or not
   */
  public static function getProductIdBySlug()
  {
    $endSlug = self::get_end_slug();
    $args = array(
      'post_type' => 'products',
      'posts_per_page' => 1,
      'slug' => $endSlug
    );
    $ret = new WP_Query( $args );
    return isset($ret->posts[0]->ID) ? $ret->posts[0]->ID : 0;
  }
  /**
   * Are we on a tag page and is the page a tag and not just /tag/
   * --
   * @return Bool are we on a tag search page.
   */
  public static function searchingByTag()
  {
    if( strstr($_SERVER['REQUEST_URI'],'/tag/')){
      return self::get_end_slug() != 'tag' ? true : false;
    }
    return false;
  }
  /**
   * Get a product title using the slug
   * --
   * @return [type] [description]
   */
  public static function getProductTitleBySlug()
  {
    $endSlug = self::get_end_slug();
    $args = array(
      'post_type' => 'products',
      'posts_per_page' => 1,
      'slug' => $endSlug
    );
    $ret = new WP_Query( $args );
    return isset($ret->posts[0]->post_title) ?$ret->posts[0]->post_title : false;
  }
  /**
   * Get the tag type = Product, solutions, Applications or Just a tag
   * --
   * Look at the child - get the parent id, use that id to query the parent and get the slug.
   * --
   * @return [type] [description]
   */
  public static function getTagType()
  {
    $slug = self::get_end_slug();
    $child = get_terms( 'post_tag', array(
      'hide_empty'  => false,
      'slug'        => $slug
    ));
    if( isset($child[0]->parent) )
    {
      $parent = get_terms( 'post_tag', array(
        'hide_empty'  => false,
        'term_taxonomy_id' => $child[0]->parent
      ));
      return isset($parent[0]->slug) ? $parent[0]->slug : false;
      d( $parent );
    }
    return false;
  }
  /**
   * Get the solutions specific page
   * --
   * @param  string $slug [description]
   * @return [type]       [description]
   */
  public static function getSolutionsPage($slug = "")
  {
    $sols = get_page_by_path('solutions');
    $args = array(
      'post_type'      => 'page',
      'posts_per_page' => 1,
      'post_parent'    => $sols->ID,
      'name'            => $slug,
    );
    $ret = new WP_Query( $args );
    return isset($ret->posts[0]->ID) ? $ret->posts[0] : false;
  }
  /**
   * Remove the HTTPS in the string.
   * --
   * @param  string $str [description]
   * @return [type]      [description]
   */
  public static function removeHTTPS($str = "")
  {
    return str_replace(array('http://', 'https://'), (''), $str);
  }
  //end
}










// --> to do -- the below can go into the above...











/**
 * Get a product by slug
 * --
 * @var [type]
 */
if(!function_exists("getProductBySlug")){
  function getProductBySlug($product_slug = ""){
    if(empty($product_slug)){
      return false;
    }
    $args = array(
      'slug'        => $product_slug,
      'post_type'   => 'products',
      'post_status' => 'publish',
      'numberposts' => 1
    );
    $product = get_posts($args);
    if(empty($product)){
      return false;
    }
    return $product;
  }
}

/**
 * Display the two types of Dimension tables tables.
 * --
 */
if(!function_exists("display_spec_tables")){
  function display_spec_tables($dimensions = array() )
  {
    if(!isset($dimensions['body'])){
      return;
    }

    //re arange the table for the smaller view
    $tds = array();
    foreach($dimensions['body'] as $row){
      $tmp = array();
      foreach($row as $tr){
        $tmp[] = $tr['c'];
      }
      $tds[] = $tmp;
    }

    echo '<div class="col-xs-12 col-lg-6">';

      //table right col small view
      echo '<table class="table table-hover visible-xs dim-table">';

      /*
      if ( $dimensions['header'] ) {

            echo '<thead>';

                echo '<tr>';

                    foreach ( $dimensions['header'] as $th ) {

                        echo '<th>';
                            echo $th['c'];
                        echo '</th>';
                    }

                echo '</tr>';

            echo '</thead>';
        }
        */

      $td_count = count($tds[0]);
      $col_count = count($tds);

      for($x = 0; $x < $td_count; $x++){
        echo '<tr>';
        for($y = 0; $y < $col_count; $y++){
            echo '<td>'.$tds[$y][$x].'</td>';
          }
        echo '</tr>';
      }
      echo '</table>';

      //table right col large view
      echo '<table class="table table-hover hidden-xs specs-info-table">';
      if ( $dimensions['header'] ) {

            echo '<thead>';

                echo '<tr>';

                    foreach ( $dimensions['header'] as $th ) {

                        echo '<th>';
                            echo $th['c'];
                        echo '</th>';
                    }

                echo '</tr>';

            echo '</thead>';
        }
      $r = 0;
      foreach($dimensions['body'] as $row){
        echo '<tr>';
          $c = 0;
          foreach($row as $tr){
              echo '<td>' . $tr['c'] . '</td>';
            }
            $c++;
        echo '</tr>';
        $r++;
      }
      echo '</table>';

    echo '</div>';

  }
}
/**
 * Extract and display spec images from Editor gallery
 * --
 */
if(!function_exists("display_spec_images_from_gallery")){
  function display_spec_images_from_gallery($image = "" , $has_dimensions = false){
    preg_match_all('/<img[^>]+>/i', $image, $images);
    if(isset($images[0]) && !empty($images[0]))
    {
      foreach($images[0] as $img)
      {
        if(count($images[0]) == 1){
          echo '<div class="col-xs-12">';
          echo $img;
          echo '</div>';
        }else{
          echo '<div class="col-xs-12 col-lg-6">';
          echo $img;
          echo '</div>';
        }
      }
    }
  }
}
/**
 * Build the footerLinks
 * --
 */
if(!function_exists('build_footer_list')){
  function build_footer_list($parent_slug = false){
    $links = getFooterProducts($parent_slug);
    $page = get_page_by_path( 'solutions/' . $parent_slug );

    $my_wp_query = new WP_Query();
    $args = array('post_type' => 'page', 'posts_per_page' => '-1');

    $all_wp_pages = $my_wp_query->query($args);
    //this does not work with the menu ordering
    //$children = get_page_children( $page->ID , $all_wp_pages );
    //this does work with the menu ordering
    $children  = get_posts( array(
      'post_type' => 'page',
      'post_parent' => $page->ID,
    ));

    echo '<div class="col-md-3 col-sm-6"><ul>';

    if(is_object($page)){
      echo '<li><a href="'.get_the_permalink($page->ID).'">'. sgt::non_translate($page->post_title) .'</a></li>';
    }

    if(!empty($children)){
      foreach( $children as $child){
        if(preg_match('/product_parent.php/', get_page_template_slug($child->ID), $matches)){
          echo '<li><a href="'.get_the_permalink($child->ID).'">'.$child->post_title.'</a></li>';
        }
      }
    }

    if(!empty($links)){
      foreach( getFooterProducts($parent_slug) as $link ){
        echo '<li><a href="'.$link['link'].'">'.sgt::non_translate($link['title']).'</a></li>';
      }
    }
    echo '</ul></div>';
  }
}
/**
 * Get the footer Links
 * --
 */
if(!function_exists('getFooterProducts')){
  function getFooterProducts($parent_slug = false)
  {
    $ret = array();
    $products = array();
    //no parent slug == filter all products by top level
    if(!$parent_slug)
    {
      $product_categories = products_solutions::get_product_categories();
      foreach($product_categories as $parent_slug){
        $products[] = products_solutions::get_products_by_tag($parent_slug);
      }
    //parent slug == products under that parent
    }else{
      $products[] = products_solutions::get_products_by_tag($parent_slug);
    }
    //foreach product grab perman link and title
    foreach( $products as $product_group){
      foreach($product_group->posts as $post){
        $ret[] = array(
          'link'  => get_the_permalink($post->ID),
          'title' => get_the_title($post->ID)
        );
      }
    }
    return $ret;
  }
}
/**
 * Get the news / stories by tag slug
 * --
 */
if(!function_exists("get_related_stories")){
  function get_related_stories( $slug = '' ){
    if(!$slug || empty($slug) ){
      return false;
    }
    $args = array(
      'tag'       => $slug,
      'post_type' => 'post',
      'category_name' => 'news'
    );
    return new WP_Query( $args );
  }
}
/**
 * Filter out the dupes
 * --
 */
if(!function_exists("unique_application")){
  function unique_application( $applications = array() ){
    $applications_used = array();
    $unique_applications = array();
    foreach( $applications as $item){
    	if(is_array( $item )){
    		foreach($item as $sub_item){
    			if(!in_array($sub_item->ID, $applications_used)){
    				$unique_applications[] = $sub_item;
    			}
    			$applications_used[] = $sub_item->ID;
    		}
    	}else{
        if( isset($item->ID)){
          if( !in_array($item->ID, $applications_used)){
       			$unique_applications[] = $item;
       		}
       		$applications_used[] = $item->ID;
        }
      }
    }
    return $unique_applications;
  }
}
/**
 * Debugfunction - much quiker to type!
 * --
 */
if(!function_exists("d")){
  function d( $data = array(), $die = true ){
    echo '<pre>'.print_r($data, true).'</pre>';
    if($die){
      die();
    }
  }
}
/**
 * Is the post tagged as being in a aproduct category ?
 * --
 */
if(!function_exists('is_product_category')){
  function is_product_category(){
    $end_slug = products_solutions::get_end_slug();
    $product_categories = products_solutions::get_product_categories();
    if(in_array($end_slug, $product_categories)){
      return true;
    }
    return false;
  }
}
/**
 * Get the product by tag.
 * --
 */
if(!function_exists('get_products_by_category')){
  function get_products_by_category($category_name = FALSE){
    if(!$category_name){
      return array();
    }
    $args = array(
      //'cat' => '54',
      //'post_type' => 'post'
      'cat' => '17',
    );
    return new WP_Query( $args );
  }
}





/**
 * Add tags, categories etc.. to products and marketoing depts
 * --
 * @var [type]
 */
add_action( 'init', 'gp_register_taxonomy_for_products' );
if(!function_exists('gp_register_taxonomy_for_products')){
  function gp_register_taxonomy_for_products() {
    //products
    register_taxonomy_for_object_type( 'post_tag', 'products' );
  	register_taxonomy_for_object_type( 'category', 'products' );
    //Marketing
    register_taxonomy_for_object_type( 'post_tag', 'marketingdepartment' );
  	register_taxonomy_for_object_type( 'category', 'marketingdepartment' );
  };
}





/**
 * OSX is not letteing me send emails.... weird!!!
 * turn off the sending part
 * --
 */
// add_filter('wpcf7_skip_mail','my_skip_mail');
// function my_skip_mail($f){
//    $submission = WPCF7_Submission::get_instance();
//    return true;
// }
