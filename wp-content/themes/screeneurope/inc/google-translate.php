<?php
/**
* Screen Google translate class
* --
* to use:
* --
* <?php echo sgt::code();?>
* --
*/
class sgt
{
	/**
	* Return the google trsnalte code as a test string that can be used in a template.
	* --
	* @return [type] [description]
	*/
	public static function code()
	{
		return '
		<div id="google_translate_element"></div>
		<script type="text/javascript">
		function googleTranslateElementInit() {
		new google.translate.TranslateElement({pageLanguage: \'en\', autoDisplay: true, multilanguagePage: true}, \'google_translate_element\');
		}
		</script>
		<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
		';
	}
	public static function non_translate($content = "")
	{

		return $content;

		if( is_array($content) || FALSE === $content || is_null($content) ){
			return $content;
		}

		$tmp_content = explode(' ', $content);

		$find = array(
			'L350UV+',
			'screen',
			'Screen',
			'SCREEN',
			'PlateRite',
			'Truepress',
			'Truepress Jet',
			'Jet520HD',
			'Jet520NX',
			'Jet520ZZ',
			'Jet520EX',
			'Jet520',
			'Truepress sc ink',
			'L350UV',
			'Jetl350',
			'Jetl350UV+LM',
			'Jet',
			'High-Speed',
			'Inkjet',
			'equios',
			'Equios',
			'EQUIOS'
		);
		$replace = array(
			'<span class="skiptranslate">L350UV+</span>',
			'<span class="skiptranslate">screen</span>',
			'<span class="skiptranslate">Screen</span>',
			'<span class="skiptranslate">SCREEN</span>',
			'<span class="skiptranslate">PlateRite</span>',
			'<span class="skiptranslate">Truepress</span>',
			'<span class="skiptranslate">Truepress Jet</span>',
			'<span class="skiptranslate">Jet520HD</span>',
			'<span class="skiptranslate">Jet520NX</span>',
			'<span class="skiptranslate">Jet520ZZ</span>',
			'<span class="skiptranslate">Jet520EX</span>',
			'<span class="skiptranslate">Jet520</span>',
			'<span class="skiptranslate">Truepress sc ink</span>',
			'<span class="skiptranslate">L350UV</span>',
			'<span class="skiptranslate">Jetl350</span>',
			'<span class="skiptranslate">Jetl350UV+LM</span>',
			'<span class="skiptranslate">Jet</span>',
			'<span class="skiptranslate">High-Speed </span>',
			'<span class="skiptranslate">Inkjet</span>',
			'<span class="skiptranslate">equios</span>',
			'<span class="skiptranslate">Equios</span>',
			'<span class="skiptranslate">EQUIOS</span>'
		);

		//d($tmp_content, 0);

		$stops = array(
			'L350UV+"',
			'/>',
			'L350UV+LM"',
			'L350UV',
			'L350UV+',
			'L350UV+LM',
			'Jet'
		);

		foreach($tmp_content as &$item)
		{
			if(preg_match('/src|title|href|alt|btn/', $item, $matches) || in_array($item, $stops) ){

			}else{
				$item = str_replace($find, $replace, $item);
			}
		}


		$content = implode(' ', $tmp_content);
		return $content;
	}
}
