<?php
/**
 * Add post types to search
 * --
 */
/*
if(!function_exists('rc_add_cpts_to_search')){
	function rc_add_cpts_to_search($query)
	{
		// Check to verify it's search page
		if( is_search() ) {
			// Get post types
			$post_types = get_post_types(array('public' => true, 'exclude_from_search' => false), 'objects');
			$searchable_types = array();
			// Add available post types
			if( $post_types ) {
				foreach( $post_types as $type) {
					$searchable_types[] = $type->name;
				}
			}
			$query->set( 'post_type', $searchable_types );
		}
		return $query;
	}
	add_action( 'pre_get_posts', 'rc_add_cpts_to_search' );
}
*/
/**
 * Set Posts per page
 * --
 */
if(!function_exists('set_posts_per_page')){
	add_action( 'pre_get_posts',  'set_posts_per_page'  );
	function set_posts_per_page( $query )
	{
		if(is_admin()){
			return $query;
		}
		global $wp_the_query;
		$query->set( 'posts_per_page', 10 );
	  return $query;
	}
}
