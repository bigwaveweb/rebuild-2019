<!DOCTYPE html>
<html>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.css" />
	<link rel="stylesheet" href="/wp-content/themes/screeneurope/assets/css/styles.css<?php echo '?v='.rand(0, 99999); ?>">
	<?php
    if (is_search()) {
        echo '<meta name="robots" content="noindex, nofollow">';
    }
    ?>
<meta name="google-site-verification" content="XF1oWmxtRfLFFilOrwlfaHUyYo_vRN3ptr7qH7siTz0" />
</head>
<body <?php body_class(); ?>>
	<?php if (function_exists('gtm4wp_the_gtm_tag')) {
        gtm4wp_the_gtm_tag();
    } ?>
	<div id="page" class="site">
		<header id="masthead" class="site-header fixed-top top" role="banner">
			<div id="search" class="collapse" aria-expanded="true" style="">
				<div class="container">
					<div class="row">
						<form action="/" method="GET">
							<div class="col-md-10 col-sm-9">
							<input type="text" name="s" value="" id="s" placeholder="Type what you would like to find here."></div>
							<div class="col-md-2 col-sm-3"><input type="submit" value="SEARCH"></div>
						</form>
					</div>
				</div>

			</div>
			<?php if (has_nav_menu('top')) : ?>
				<div class="navigation-top">
					<div class="wrap">
						<?php get_template_part('template-parts/navigation/navigation', 'top'); ?>
					</div>
				</div>

			<?php endif; ?>
		</header>


		<?php if (is_home() || is_front_page()) : ?>
			<section id="slideshow" class="widget-section bg-grey-darkest">
				<div class="slideshow__container">
					<?php
						if(ICL_LANGUAGE_CODE=='fr') 
						{
							$slider = "homepage-french";
						} 
						elseif (ICL_LANGUAGE_CODE=='de')
						{
							$slider = "homepage-german";
						} 
						else 
						{
							$slider = "homepage";
						}


						echo do_shortcode('[rev_slider alias="'. $slider .'"]');						
                    ?>
				</div>
			</section>
		<?php endif; ?>
		<div class="site-content-contain">
			<div id="content" class="site-content">
