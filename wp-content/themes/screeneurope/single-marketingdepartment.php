<?php
get_header(); ?>
<div class="wrap" id="marketingdepartment-post">
	<div class="container">
		<?php
		if(have_posts()){
			while(have_posts()){
				the_post();

				$id				= get_the_ID();
				$slug 			= '/marketingdepartment/' . get_post_field( 'post_name', $id );
				$position 		= get_field('customer_position', $id);
				$company 		= get_field('customer_company', $id);
				$location 		= get_field('customer_location', $id);
				$image_url 		= get_the_post_thumbnail_url($id, 'medium');
				$focus 			= get_field('customer_focus', $id);
				$introduction	= get_field('introduction', $id);
				$success 		= get_field('customer_success', $id);
				$news_link      = get_field('news_link', $id);
				$testimonials 	= products_solutions::getQuotesForCustomer($id);
				$quote			= products_solutions::randomSlice($testimonials);
				$quote_product	= products_solutions::getProductByQuote(isset($quote[0]->ID) ? $quote[0]->ID : FALSE);
				$post_tags		= products_solutions::getAlltagsForPost($id);
				$news_items 	= products_solutions::getStoriesByTag($post_tags);
		?>
		<?php /*
		<section class="row hidden-xs">
			<div class="col-xs-12">
				<div class="arrow-header first-heading no-bottom">
					<h1>Meet Screen Customers:</h1>
					<h2>Our Best Marketing Message</h2>
				</div>
			</div>
		</section>
		*/?>
		<?php /*
		<section class="row visible-xs">
			<div class="col-xs-12">
				<h1>Meet Screen Customers:</h1>
				<h2 class="bmm">Our Best Marketing Message</h2>
			</div>
		</section>
		*/?>
		<section class="row person-details">
			<div class="col-xs-12 col-sm-6">
				<div class="row">
					<div class="col-xs-5 col-tn-6 col-sm-5">
						<img class="person-img" src="<?php echo get_the_post_thumbnail_url( get_the_ID(), 'medium');?>" alt="Image of <?php echo get_the_title();?>" class="img-responsive" />
					</div>
					<div class="col-xs-7 col-tn-6 col-sm-7">

						<h1>
							<small class="introducing">Introducing:</small>
							<span class="person"><?php echo get_the_title();?></span>
						</h1>

						<p class="details">
							<?php echo !empty($position) ? $position . '<br>' : ''?>
							<?php echo !empty('company') ? '<b>'.$company.'</b>':'';?>
							<?php echo !empty('location') ? '<br>'.$location: '';?>
						</p>
						<?php
						$file = get_field('case_study');
						if(isset($file['url']) && !empty($file['url'])){
							echo '<a class="btn btn-default" target="_blank" href="'.$file['url'].'">Download case study &raquo;</a>';
						}
						?>
					</div>
				</div>

				<?php if(!empty($quote_product)){ ?>
					<div class="row visible-sm">
						<div class="col-xs-12">
							<h3 class="product-heading"><a href="<?php echo get_permalink($quote_product->ID);?>"><?php echo get_the_title($quote_product->ID);?></a></h3>
							<p><?php echo get_the_title($quote_product->ID);?></p>
							<div class="row">
								<div class="col-xs-10 col-xs-offset-1">
									<p><a href="<?php echo get_permalink($quote_product->ID);?>"><img src="<?php echo get_the_post_thumbnail_url( $quote_product->ID, 'medium');?>" class="img-responsive" alt="Image of <?php echo get_the_title($quote_product->ID);?>"></a></p>
								</div>
							</div>
							<p><a href="<?php echo get_permalink($quote_product->ID);?>" class="btn btn-default">Read More &raquo;</a></p>
						</div>
					</div>
				<?php } ?>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 ">


				<?php
				if( !empty( $introduction ) )
				{
				?>
					<p><?php echo $introduction;?></p>
				<?php
				}
				?>

				<?php if(!empty($focus)){ ?>
					<h3 class="section-heading">Focus:</h3>
					<p><?php echo products_solutions::subWords(strip_tags($focus));?></p>
				<?php } ?>

				<?php if(!empty($success)){ ?>
					<h3 class="section-heading">Success:</h3>
					<p><?php echo products_solutions::subWords(strip_tags($success));?></p>
				<?php } ?>

				<?php
				if( !empty( $news_link ) )
				{
				?>
					<p><a class="btn btn-default" href="<?php echo $news_link;?>">Read more...</a></p>
				<?php
				}
				?>

				<?php if(isset($quote[0]) && !empty($quote[0]->post_content) ){ ?>
					<h3 class="section-heading">Quote:</h3>
					<p><?php echo products_solutions::subWords(strip_tags(isset($quote[0]) ? $quote[0]->post_content : ''));?></p>
				<?php } ?>

			</div>
			<?php if(!empty($quote_product)){ ?>
				<div class="col-xs-12 hidden-sm col-md-2">
					<h3 class="product-heading">
						<a href="<?php echo get_permalink($quote_product->ID);?>"><?php echo get_the_title($quote_product->ID);?></a>
					</h3>
					<p><?php echo get_the_title($quote_product->ID);?></p>
					<div class="row">
						<div class="col-xs-10 col-xs-offset-1">
							<p><a href="<?php echo get_permalink($quote_product->ID);?>"><img src="<?php echo get_the_post_thumbnail_url( $quote_product->ID, 'medium');?>" class="img-responsive" alt="Image of <?php echo get_the_title($quote_product->ID);?>"></a></p>
						</div>
					</div>
					<p><a href="<?php echo get_permalink($quote_product->ID);?>" class="btn btn-default">Read More &raquo;</a></p>
				</div>
			<?php } ?>
		</section>
		<?php

		if ( !empty($news_items) ) {


			// d($news_items, 0);

			echo '<div class="marketing-stories-container">';
				echo '<div class="row">';
				foreach($news_items as $news ){

					setup_postdata( $news );

					//$news = get_post( $news_item->ID );

					$formatVideo = products_solutions::isVideoFormat( $news->ID );
					if(!$formatVideo){ ?>
						<div class="col-sm-6 story">
							<h3 class="light">
								<a href="<?php echo get_the_permalink($news->ID);?>"><?php echo get_the_title($news->ID);?></a>
							</h3>
							<div class="row">
								<?php
								$image = get_the_post_thumbnail_url($news->ID);
								if(!empty($image)){
								?>
									<div class="col-md-6">
										<div class="row">
											<div class="col-sm-11">
												<p>
													<a href="<?php echo get_the_permalink($news->ID);?>">
														<img src="<?php echo get_the_post_thumbnail_url($news->ID);?>" alt="Image from <?php echo get_the_title($news->ID);?>">
													</a>
												</p>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<p><?php echo truncate_string(strip_shortcodes(get_the_excerpt($news->ID)));?></p>
										<p><a href="<?php echo get_the_permalink($news->ID);?>" class="btn btn-default">Read More&nbsp;&raquo;</a></p>
									</div>
								<?php } else { ?>
									<div class="col-xs-12">
										<p><?php echo truncate_string(strip_shortcodes(get_the_excerpt($news->ID)));?></p>
										<p><a href="<?php echo get_the_permalink($news->ID);?>" class="btn btn-default">Read More&nbsp;&raquo;</a></p>
									</div>
								<?php } ?>
							</div>
						</div>
					<?php } else { ?>
						<div class="col-sm-6">
							<h3 class="light"><?php echo get_the_title($news->ID);?></h3>
							<div class="row">
								<div class="col-xs-12">
									<div class="embed-responsive embed-responsive-16by9">
										<?php
										$content = apply_filters( 'the_content', get_post_field('post_content', $news->ID ) );
										$video = false;
										if ( false === strpos( $content, 'wp-playlist-script' ) ) {
											$video = get_media_embedded_in_content( $content, array( 'video', 'object', 'embed', 'iframe' ) );
										}
										if(isset($video[0])){
											echo $video[0];
										}
										?>
									</div>
								</div>
							</div>
							<p><?php echo truncate_string( strip_shortcodes( get_the_excerpt($news->ID) ));?></p>
						</div>
					<?php
					}
				}
								echo '</div>';
							echo '</div>';
						}
					products_solutions::getTheMarketingPeeps( $id );
				}
			}?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
