<?php
get_header();
?>
<div class="wrap">
	<?php if ( is_home() && ! is_front_page() ) : ?>
		<header class="page-header">
			<h1 class="page-title"><?php single_post_title(); ?></h1>
		</header>
	<?php else : ?>
		<header class="page-header">
			<h2 class="page-title"><?php _e( 'Posts', 'screeneurope' ); ?></h2>
		</header>
	<?php endif; ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main bg-white" role="main">
			<div class="container">
				<div class="row">
					<?php
					if ( have_posts() ) :
						while ( have_posts() ) : the_post();
							get_template_part( 'template-parts/post/content', get_post_format() );
						endwhile;
						the_posts_pagination();
					else :
						get_template_part( 'template-parts/post/content', 'none' );
					endif;
					?>
				</div>
			</div>
		</main>
	</div>
	<?php get_sidebar(); ?>
</div>
<?php get_footer();
