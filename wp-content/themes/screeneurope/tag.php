<?php
get_header();
$the_tag = get_queried_object();
$tag_slug = $the_tag->slug;
?>
<div class="wrap tags" id="archive">
	<div class="container">
		<div class="row">
			<?php if ( have_posts() ) : ?>
			<div class="col-md-12">
				<header class="page-header">
					<?php
					the_archive_title( '<h1 class="page-title tag__title">', '</h1>' );
					the_archive_description( '<div class="taxonomy-description">', '</div>' );
					?>
				</header>
			</div>
			<?php endif; ?>
			<div class="col-xs-12 category-news__container">
				<div id="primary" class="content-area category-news">
					<main id="main" class="site-main" role="main">
						<?php
						if ( have_posts() ) : ?>
							<div class="<?php echo($tag_slug == 'video') ? 'col-xs-12' : 'col-sm-7';?>">
								<div class="row">
								<?php
								$count = 0;
								while ( have_posts() ) : the_post(); ?>
									<div class="<?php echo($tag_slug == 'video') ? 'col-xs-12 col-sm-6' : 'col-xs-12';?>">
									<?php
										$formatVideo = get_post_format() === "video";
										if ($formatVideo) :
											get_template_part('template-parts/post/content-video', get_post_format());
										else :
											get_template_part('template-parts/post/content-excerpt', get_post_format());
										endif;
									?>
									</div>
									<?php
									//--> Todo --- need this to work with the loading below.
									/*
									$count++;
									if($count > 1){
										$count = 0;
										echo '<div class="col-xs-12"></div>';
									}
									*/
								endwhile;
								echo '</div>';
								if($formatVideo){
								?>
									<div class="load-more-container">
										<div class="tpl"></div>
										<div class="posts"></div>
										<div class="col-xs-12">
											<a href="#" class="more-video">See more</a>
										</div>
									</div>
								<?php }
								if(!$formatVideo){
									the_posts_pagination( array(
										'prev_text' => '<span class="screen-reader-text">' . __( 'Previous', 'screeneurope' ) . '</span>',
										'next_text' => '<span class="screen-reader-text">' . __( 'Next', 'screeneurope' ) . '</span>',
										'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( false, 'screeneurope' ) . ' </span>',
									));
								}?>
							</div>
							<?php else :
								echo '
								<div class="col-sm-7">
									<div class="row">
								';
										get_template_part( 'template-parts/post/content', 'none' );
								echo '
									</div>
								</div>
								';
							endif; ?>
						</div>
						<?php if($tag_slug != 'video'){ ?>
							<aside class="col-sm-4 col-sm-offset-1">
								<?php

								//get the parent tag type - i.e the type
								$tag_type = products_solutions::getTagType();

								//switch the type
								switch($tag_type)
								{
									case "solutions":
										$posts = products_solutions::getSolutionsPage( products_solutions::get_end_slug() );
										if(isset($posts->ID)){
											echo '
											<h3 class="light" style="margin-bottom:0"><a href="'.get_the_permalink($posts->ID).'">'.get_the_title($posts->ID).'</a></h3>
											<p>'.get_field('section_sub_title', $posts->ID).'</p>
											<p><a href="'.get_the_permalink($posts->ID).'" class="btn btn-default">'.get_the_title($posts->ID).' &raquo;</a></p>
											<p>&nbsp;</p>
											';
										}
										dynamic_sidebar( 'sidebar-news-1' );
									 	break;

									case "applications":
										echo '<h3 class="light">Topics</h3>';
										echo "<p><b>You're currently viewing News & Media related to <i>'".products_solutions::slugToTitle( products_solutions::get_end_slug() ).".'</i></b><br><br>";
										echo '<a href="/category/news/" class="btn btn-default">See All News &amp; Media &raquo;</a></p><br><br>';
									 	break;

									case "products":
										//get the id by the tag - i.e get the id for truepress-jet520nx
										$glob_id = products_solutions::getProductIdBySlug();
										//then display the related products
										products_solutions::getRelatedProductsHtml($glob_id, 3);
										//show the list of tagged sidebars
										if(products_solutions::searchingByTag() ){
											//if searching by product and have a valid product title
											$productTitle = products_solutions::getProductTitleBySlug();
											if($productTitle){
												echo '<h3 class="light">Topics</h3>';
												echo "<p><b>You're currently viewing News & Media related to <i>'".$productTitle.".'</i></b><br><br>";
												echo '<a href="/category/news/" class="btn btn-default">See All News &amp; Media &raquo;</a></p>';
											}else{
												dynamic_sidebar( 'sidebar-news-1' );
											}
										}else{
											dynamic_sidebar( 'sidebar-news-1' );
										}
								 		break;

									default:
										dynamic_sidebar( 'sidebar-news-1' );
										break;
								}
								show_all_tags_by_parent();
								?>
							</aside>
						<?php } ?>
					</main>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer();
