<?php
/* Template Name: Product Parent */
get_header(); ?>
<div class="wrap">
	<div class="container">
		<div class="row">
			<div id="primary" class="content-area">
				<main id="main" class="site-main bg-white" role="main">
					<div class="col-md-12">
						<?php
						$applications = array();
						while ( have_posts() ) : the_post();
							get_template_part( 'template-parts/products/product-page-parts' );
						endwhile;
						?>
					</div>
				</main>
			</div>
		</div>
	</div>
</div>
<?php get_footer();
