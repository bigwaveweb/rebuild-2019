<?php
get_header();
?>
<div class="wrap">
	<header class="page-header">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<?php /*
					<h2 class="page-title"><?php _e( 'Posts', 'screeneurope' ); ?></h2>
					*/?>
				<h2 class="page-title">That page can not be found</h2>
				</div>
			</div>
		</div>
	</header>
	<div id="primary" class="content-area">
		<main id="main" class="site-main bg-white" role="main">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<p>Return to the <a href="/">Homepage</a></p>
						<?php
						/*
						if ( have_posts() ) :
							while ( have_posts() ) : the_post();
								get_template_part( 'template-parts/post/content', get_post_format() );
							endwhile;
							the_posts_pagination();
						else :
							get_template_part( 'template-parts/post/content', 'none' );
						endif;
						*/
						?>
					</div>
				</div>
			</div>
		</main>
	</div>
	<?php //get_sidebar(); ?>
</div>
<?php get_footer();
