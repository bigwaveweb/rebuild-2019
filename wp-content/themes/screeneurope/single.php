<?php
get_header(); ?>
<div class="wrap" id="news-post">
	<div class="container">
		<div class="row">
			<div id="primary" class="content-area">
				<div class="col-md-12">
					<header class="page-header">
						<h4 class="title__news">Screen Europe News &amp; Media</h4>
					</header>
				</div>
				<div class="col-xs-12 category-news__container">
					<div id="primary" class="content-area category-news">
						<main id="main" class="site-main" role="main">
							<?php
							if ( have_posts() ) : ?>
							<div class="col-md-7">
								<?php
								$glob_id = false;
								while ( have_posts() ) : the_post();
									get_template_part( 'template-parts/post/content', get_post_format() );
									$glob_id = get_the_ID();
								endwhile;
								?>
							</div>
							<?php endif; ?>
							<div class="col-md-4 col-md-offset-1">
								<?php dynamic_sidebar( 'sidebar-news-single' );?>
								<?php products_solutions::getRelatedProductsHtml($glob_id, 2);?>
								<aside>
									<?php
										$thisPostId 	= get_the_id();
										$tagsByTagParent = wp_get_post_tags($thisPostId, array('hide_empty'=>false,  'orderby'=> 'asc', 'parent'=>39));
									if (!empty($tagsByTagParent)) : ?>
									<h3 class="widget-title tag__parent">Tags</h3>
									<?php
										foreach ($tagsByTagParent as $key => $tag) :
											$tag_link = get_tag_link( $tag->term_id );
											echo "<span class=\"label label-default\"><a href=\"{$tag_link}\" class=\"{$tag->slug}\">{$tag->name}</a></span> ";
										endforeach;
									endif;
									?>
								</aside>
								<?php dynamic_sidebar( 'sidebar-news-single-bottom' ); ?>
							</div>
						</main>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
