<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header clearfix">
		<?php the_title( '<h1 class="entry-title pull-left">', '</h1>' ); ?>
	</header><!-- .entry-header -->
	<div class="entry-content">
		<?php
			echo apply_filters( 'the_content', sgt::non_translate(get_the_content()));
		?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
