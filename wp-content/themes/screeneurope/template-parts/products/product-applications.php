<?php
//have not used global in yonks!! :)
global $applications;
global $glob_id;
$unique_applications = unique_application($applications);
$odd = true;

if(!empty($unique_applications))
{
	//get the slug
	$endSlug = products_solutions::get_end_slug();
	//use the slug to get the page
	$the_page = get_page_by_path('solutions/' . $endSlug);
	//display the codeszzz......
	echo '<div class="product-applications">';
	if( ICL_LANGUAGE_CODE == 'de') {
		echo '<h2 class="product-list-app-header" id="applications">ANWENDUNGSMÖGLICHKEITEN der '. sgt::non_translate(get_the_title($the_page->ID)) .'</h2>';
	} else {
		echo '<h2 class="product-list-app-header" id="applications">APPLICATIONS for SCREEN '. sgt::non_translate(get_the_title($the_page->ID)) .'</h2>';
	}
}

foreach($unique_applications as $item){
	if($odd){ ?>
		<div class="row">
			<div class="application-item">
				<div class="col-sm-5">
					<?php $image_url = get_the_post_thumbnail_url($item->ID); ?>
					<p><img src="<?php echo !empty($image_url) ? $image_url : '' ;?>" alt="Image of <?php echo get_the_title();?>" class=""></p>
				</div>
				<div class="col-sm-7">
					<h2 class="light"><?php echo sgt::non_translate($item->post_title);?></h2>
					<h4 class="light flush"><?php echo sgt::non_translate($item->post_content);?></h4>
				</div>
			</div>
		</div>
		<?php $odd = false;
	}else{
		$odd = true; ?>
		<div class="row">
			<div class="application-item">
				<div class="col-sm-5 visible-xs">
					<?php $image_url = get_the_post_thumbnail_url($item->ID); ?>
					<p><img src="<?php echo !empty($image_url) ? $image_url : '' ;?>" alt="Image of <?php echo get_the_title();?>" class=""></p>
				</div>
				<div class="col-sm-7">
					<h2 class="light"><?php echo sgt::non_translate($item->post_title);?></h2>
					<h4 class="light flush"><?php echo sgt::non_translate(sgt::non_translate($item->post_content));?></h4>
				</div>
				<div class="col-sm-5 hidden-xs">
					<?php $image_url = get_the_post_thumbnail_url($item->ID); ?>
					<p><img src="<?php echo !empty($image_url) ? $image_url : '' ;?>" alt="Image of <?php echo get_the_title();?>" class=""></p>
				</div>
			</div>
		</div>
	<?php }
}

if(!empty($unique_applications)){
	echo '</div>';
}
?>
