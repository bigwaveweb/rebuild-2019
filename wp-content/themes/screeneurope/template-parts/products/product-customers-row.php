<?php
global $customer;

if(isset($_REQUEST['customer']) && !empty($_REQUEST['customer'])){
	$customer = $_REQUEST['customer'];
}

$slug 			= '/marketingdepartment/' . get_post_field( 'post_name', $customer->ID );
$position 	= get_field('customer_position', $customer->ID);
$company 		= get_field('customer_company', $customer->ID);
$location 	= get_field('customer_location', $customer->ID);
$image_url 	= get_the_post_thumbnail_url($customer->ID, 'full');
?>

<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 text-center product-customers">
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1">
			<div class="img-div">
					<a href="<?php echo $slug;?>">
				 		<img src="<?php echo !empty($image_url) ? $image_url : '/assets/img/missing.png' ;?>" alt="Image of <?php echo $customer->post_title;?>">
					</a>
			</div>
		</div>
	</div>
	<div class="info">
		<p>
			<a href="<?php echo $slug;?>"><?php echo $customer->post_title;?><br>
				<?php
				if($position){
					echo '<small><i>'.$position.'</i></small><br>';
				}
				if($company){
					echo '<small><b>'.$company.'</b></small><br>';
				}
				if($location){
					echo '<small><b>'.$location .'</b></small><br>';
				}
				?>
			</a>
		</p>
	</div>
</div>
