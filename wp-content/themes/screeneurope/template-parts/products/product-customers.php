<?php
if( isset($_REQUEST['product_ids'])) {
$customers = products_solutions::getMultipleProductCustomers( $_REQUEST['product_ids'], isset($_REQUEST['excluded_person']) ?  $_REQUEST['excluded_person'] : NULL);
if(is_array($customers) && !empty($customers)){
?>
	<div class="col-xs-12">
		<div class="product-customers-container">
			<?php if(isset($_REQUEST['s_page_title']) && !empty($_REQUEST['s_page_title'])) { ?>
				<?php if(ICL_LANGUAGE_CODE == 'de'){ ?>
				
					<h3 class="text-center">Meet <span class="notranslate">SCREEN</span> <?php echo $_REQUEST['s_page_title'] ;?> Customers</h3>

				<?php } elseif(ICL_LANGUAGE_CODE == 'fr'){ ?>	

					<h3 class="text-center">Témoignages <?php echo $_REQUEST['s_page_title'] ;?>  de nos clients</h3>

				<?php } else { ?>
					<h3 class="text-center">Meet <span class="notranslate">SCREEN</span> <?php echo $_REQUEST['s_page_title'] ;?> Customers</h3>
				<?php } 
			}
			?>
				<div class="section">
				<?php foreach( $customers as $customer){ $_REQUEST['customer'] = $customer ?>
					<?php get_template_part( 'template-parts/products/product-customers-row' );?>
				<?php unset($_REQUEST['customer']); } ?>
			</div>
		</div>
	</div>
<?php
	}
}
?>
