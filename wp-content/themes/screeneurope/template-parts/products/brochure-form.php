<?php

global $glob_id;
$brochure = get_field('brochure', $glob_id);
$white_paper = get_field('white_paper', $glob_id);
$title = get_the_title($glob_id);
$contact_email = get_option('admin_email');

//get the language variants
//
$brochure_fr = get_field('fr', $glob_id);
$brochure_de = get_field('de', $glob_id);

// d($brochure_fr, 0);
// d($brochure_de, 0);

//my tmp email
//
$contact_email = 'garyconstable@bigwavemedia.co.uk';
$directDownload = get_field('brochure_direct_download', $glob_id);

//Do a direct download
//

if (!empty($directDownload) && strtolower($directDownload) == 'yes') {
    if (ICL_LANGUAGE_CODE == 'de') {
        echo '<h4>Broschüre herunterladen</h4>';
        echo '<a href="'.$brochure['url'].'" target="_blank" class="btn btn-default"><span class="glyphicon glyphicon-download-alt"></span> Broschüre herunterladen (PDF)</a>';
    
    } elseif (ICL_LANGUAGE_CODE == 'fr') {

        echo '<h4>Télécharger la brochure</h4>';
        echo '<a href="'.$brochure['url'].'" target="_blank" class="btn btn-default"><span class="glyphicon glyphicon-download-alt"></span> Télécharger la brochure (PDF)</a>';
    
    } else {

        echo '<h4>Download Brochure</h4>';
        echo '<a href="'.$brochure['url'].'" target="_blank" class="btn btn-default"><span class="glyphicon glyphicon-download-alt"></span> Download Brochure (PDF)</a>';
    }

    if (!empty($brochure_related['url'])) {
        echo '<a href="'.$brochure_related['url'].'" target="_blank" class="btn btn-default"><span class="glyphicon glyphicon-download-alt"></span> Download Related Brochure (PDF)</a>';
    }
    //Use the form and request the users details.
//
} else {
    if (ICL_LANGUAGE_CODE == 'de') {
        echo '
		<h4>Broschüre herunterladen</h4>
		<p>Bitte machen Sie einige Angaben zu Ihrer Person, bevor Sie die Broschüre für '.sgt::non_translate(get_the_title($glob_id)).' herunterladen.</p>
		';

        echo do_shortcode('[contact-form-7 id="2274" title="Brochure"]');

    } elseif(ICL_LANGUAGE_CODE == 'fr') {
        
        echo '
		<h4>Télécharger la brochure</h4>
		<p>Please provide a little information about yourself before downloading the '.sgt::non_translate(get_the_title($glob_id)).' brochure.</p>
		';

        echo do_shortcode('[contact-form-7 id="2374" title="Brochure French"]');
    } else {
        
        echo '
		<h4>Download Brochure</h4>
		<p>Please provide a little information about yourself before downloading the '.sgt::non_translate(get_the_title($glob_id)).' brochure.</p>
		';

        echo do_shortcode('[contact-form-7 id="265" title="Brochure"]');
    }
}

if (isset($brochure['url'])) {
    echo '<style>#download_brochure{display:block !important;}</style>';
}

if (isset($white_paper['url'])) {
    echo '<style>#download_white_paper{display:block !important;margin-top:10px;}</style>';
}

//if we have the language show the select
if ((isset($brochure_fr['url']) && !empty($brochure_fr['url'])) || (isset($brochure_de['url']) && !empty($brochure_de['url']))) {
    echo '<style>#language_select_label{    display: block !important}</style>';
    echo '<style>#language_select{display:block !important;margin-top:10px;}</style>';
}

//if we have the specific language show the sepecific language optipon
if ((isset($brochure_fr['url']) && !empty($brochure_fr['url']))) {
    echo '<style>#language_select_fr{display:block !important;}</style>';
}

//if we have the specific language show the sepecific language optipon
if ((isset($brochure_de['url']) && !empty($brochure_de['url']))) {
    echo '<style>#language_select_de{display:block !important;}</style>';
}

/*
<div class="row">
    <div class="col-sm-2">
        <label id="language_select_label" style="display:none;">Brochure language</label>
    </div>
  <div class="col-sm-10">
        <select id="language_select" style="display:none" class="form-control" name="brochure_language_select">
            <option id="language_select_en" value="EN">EN</option>
      <option id="language_select_de" value="DE" style="display:none">DE</option>
            <option id="language_select_fr" value="FR" style="display:none">FR</option>
    </select>
  </div>
</div>
*/

?>
<form class="brochure-download-form">
	<div class="form-error"></div>
	<input class="page-title" type="hidden" value="<?php echo isset($title) ? $title : ''; ?>" />
	<input class="contact-email" type="hidden" value="<?php echo isset($contact_email) ? $contact_email : ''; ?>" />
	<input class="brochure-link" type="hidden" value="<?php echo isset($brochure['url']) ? $brochure['url'] : ''; ?>" />
	<input class="white-paper-link" type="hidden" value="<?php echo isset($white_paper['url']) ? $white_paper['url'] : ''; ?>" />
	<input class="brochure-link-fr" type="hidden" value="<?php echo isset($brochure_fr['url']) ? $brochure_fr['url'] : ''; ?>" />
	<input class="brochure-link-de" type="hidden" value="<?php echo isset($brochure_de['url']) ? $brochure_de['url'] : ''; ?>" />
</form>
