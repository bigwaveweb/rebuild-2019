<?php

/**
 * Product Style page
 * --
 */
global $applications;

get_template_part( 'template-parts/products/banner-intro' );

$products = products_solutions::get_products_by_tag( products_solutions::get_end_slug() );

$product_ids = array();

$_REQUEST['s_page_title'] = get_the_title();



if ( $products->have_posts() && count($products->posts) > 1 ) :

	echo '<div class="row"><ul class="product-list-item-small " id="products">';

	while ( $products->have_posts() ) : $products->the_post();

		get_template_part( 'template-parts/products/product-list-item-small' );

		$tmp = get_field( "applications" );

		$applications[] = $tmp;

		//save the products Id's for later use
		$_REQUEST['product_ids'][] = get_the_ID();

	endwhile;

	echo '</ul></div>';

endif;



if( count($products->posts) <= 1) {
		echo '<div class="row"><div class="col-xs-12">&nbsp;<hr>&nbsp;</div></div>';
}



/**
 * Show the child pages if they are using the cporrect template - i.e they are on the presspress section and using product template.
 * @var WP_Query
 */

$my_wp_query = new WP_Query();
$all_wp_pages = $my_wp_query->query(array('post_type' => 'page', 'posts_per_page' => '-1'));
//this does not work with the menu ordering
$children = get_page_children( get_the_ID() , $all_wp_pages );
//this does work with the menu ordering
$children  = get_posts( array(
  'post_type' => 'page',
  'post_parent' => get_the_ID(),
));
if(!empty($children)){
	echo '<div class="row"><div class="col-xs-12">&nbsp;</div><div class="col-xs-12">&nbsp;</div>';
	foreach( $children as $child){
		if(preg_match('/product_parent.php/', get_page_template_slug($child->ID), $matches)){
			?>
			<article class="col-xs-12 product-list-item" id="productidx_<?php echo $child->ID;?>">
				<div class="row">
					<header class="col-xs-12">
						<?php
						$product_title = get_the_title($child->ID);
						$product_sub_title = get_field( "subtitle", $child->ID);
						if(!empty($product_sub_title)){
							echo '<h2 class="h1 light u"><a href="'.get_the_permalink($child->ID).'">'.$product_title.' <small>'.$product_sub_title.'</small></a></h2>';
						}else{
							echo '<h2 class="h1 light u"><a href="'.get_the_permalink($child->ID).'">'.$product_title.'</a></h2>';
						}
						?>
					</header>
					<section class="col-xs-12">
						<h3 class="light flush"><?php echo get_field( "strapline", $child->ID );?></h3>
					</section>
					<?php
					$parent_image_src = false;
					$image_src = get_the_post_thumbnail_url($child->ID, 'medium');
					$parent_image = get_field('parent_image', $child->ID);
					if(!empty($parent_image) && isset($parent_image['url'])){
						$parent_image_src = $parent_image['url'];
					}
					?>
					<section class="col-sm-6 col-xs-12">
						<a href="<?php echo get_the_permalink($child->ID); ?>">
							<img src="<?php echo ( $parent_image_src !== false ) ? $parent_image_src : $image_src ?>" alt="Image of <?php echo get_the_title($child->ID);?>" />
						</a>
					</section>
					<section class="col-sm-6 col-xs-12">
						<div class="row">
							<div class="col-sm-11 col-sm-offset-1 col-xs-12 col-xs-offset-0">
								<span class="hidden-xs">
									<?php echo get_field( "intro_text", $child->ID );?>
								</span>
								<a class="btn btn-default" href="<?php echo get_the_permalink($child->ID); ?>" target="_new"><?php echo $product_title;?>&nbsp;&raquo;</a>
							</div>
						</div>
					</section>
				</div>
			</article>
			<?php
		}
	}
	echo '</div>';
}




if ( $products->have_posts() ) :

		echo '<div class="row">';

			while ( $products->have_posts() ) : $products->the_post();

				get_template_part( 'template-parts/products/product-list-item' );

			endwhile;

		echo '</div>';

endif;

get_template_part( 'template-parts/products/product-applications' );

get_template_part( 'template-parts/products/product-testimonial' );

get_template_part( 'template-parts/products/product-customers' );
