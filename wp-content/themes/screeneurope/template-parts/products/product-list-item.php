<article class="col-xs-12 product-list-item" id="productidx_<?php echo get_the_ID();?>">
	<div class="row">
		<header class="col-xs-12">
			<?php
			$product_title = sgt::non_translate(get_the_title());
			$product_sub_title = sgt::non_translate(get_field( "subtitle" ));
			if(!empty($product_sub_title)){

				if( get_field('allow_page') ){
					echo '<h2 class="h1"><a href="'.get_permalink().'">'.$product_title.' <small>'.$product_sub_title.'</small></a></h2>';
				}else{
					echo '<h2 class="h1"><a>'.$product_title.' <small>'.$product_sub_title.'</small></a></h2>';
				}
			}else{
				if( get_field('allow_page') ){
					echo '<h2 class="h1"><a href="'.get_permalink().'">'.$product_title.'</a></h2>';
				}else{
					echo '<h2 class="h1"><a>'.$product_title.'</a></h2>';
				}
			}
			?>
		</header>
		<section class="col-xs-12">
			<h3 class="light flush"><?php echo sgt::non_translate(get_field( "strapline" ));?></h3>
		</section>
		<section class="col-sm-6 col-xs-12">
			<?php if( get_field('allow_page') ){ ?>
				<a href="<?php echo get_permalink(); ?>">
					<img src="<?php echo get_the_post_thumbnail_url( null, 'full'); ?>" alt="Image of <?php echo get_the_title();?>" />
				</a>
			<?php } else { ?>
				<a>
					<img src="<?php echo get_the_post_thumbnail_url( null, 'full'); ?>" alt="Image of <?php echo get_the_title();?>" />
				</a>
			<?php }  ?>
		</section>
		<section class="col-sm-6 col-xs-12">
			<div class="row">
				<div class="col-sm-11 col-sm-offset-1 col-xs-12 col-xs-offset-0">
					<span class="hidden-xs">
						<?php echo sgt::non_translate(get_field( "intro_text" ));?>
					</span>
					<?php if( get_field('allow_page') ){ ?>
						<a class="btn btn-default" href="<?php echo get_permalink(); ?>"><?php echo sgt::non_translate($product_title);?>&nbsp;&raquo;</a>
					<?php } else {
						$brochure = get_field( "brochure");
						$brochure_related = get_field( "brochure_related");
						$brochureLink = isset($brochure['url']) ? $brochure['url'] : false;
						$brochureRelatedLink = isset($brochure_related['url']) ? $brochure_related['url'] : false;
						$brochure_related_title = get_field('brochure_related_title');
						if(ICL_LANGUAGE_CODE == 'de'){

							if($brochureLink){ 
								echo '<a class="btn btn-default" href="'.$brochureLink.'"><span class="glyphicon glyphicon-download-alt"></span> Broschüre herunterladen (PDF)</a>';
							}
							if ($brochureRelatedLink){
								echo '<h5>Download ' .$brochure_related_title . '</h5>';
								echo '<a class="btn btn-default" href="'.$brochureRelatedLink.'"><span class="glyphicon glyphicon-download-alt"></span> Broschüre herunterladen (PDF)</a>';
							}
						} elseif(ICL_LANGUAGE_CODE == 'fr'){

							if($brochureLink){ 
								echo '<a class="btn btn-default" href="'.$brochureLink.'"><span class="glyphicon glyphicon-download-alt"></span> Télécharger la brochure (PDF)</a>';
							}

							if ($brochureRelatedLink){
								echo '<h5>Download ' .$brochure_related_title . '</h5>';
								echo '<a class="btn btn-default" href="'.$brochureRelatedLink.'"><span class="glyphicon glyphicon-download-alt"></span> Télécharger la brochure (PDF)</a>';
							}

						} else {
							if($brochureLink){ 
								echo '<a class="btn btn-default" href="'.$brochureLink.'"><span class="glyphicon glyphicon-download-alt"></span> Download Brochure (PDF)</a>';
							}
							if ($brochureRelatedLink){
								echo '<h5>Download ' .$brochure_related_title . '</h5>';
								echo '<a class="btn btn-default" href="'.$brochureRelatedLink.'"><span class="glyphicon glyphicon-download-alt"></span> Download Related Brochure (PDF)</a>';
							}
						}
					}
					?>
				</div>
			</div>
		</section>
		<div class="col-xs-12 hr-div">
			<hr>
		</div>
	</div>
</article>
