<?php
$img_url = get_the_post_thumbnail_url(null, 'full');
if (!empty($img_url)) {
    ?>
<div class="row">

	<div class="col-xs-12">
	
		<div class="product-banner-container">
			<?php $banner_title = get_field('banner_title');
    if ($banner_title) {
        echo '<h1>'.$banner_title.'</h1>';
    } ?>

			<?php if (get_field('hide_banner_buttons')) {
        //no banner buttons
    } else {
        ?>
				<ul class="row big-black-buttons">
                <?php if(ICL_LANGUAGE_CODE== 'de') { ?>

                    <li class="col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1"><a class="btn" href="#products" data-scroll>Produkte</a></li>
					<li class="col-md-5 col-md-offset-0 col-sm-5 col-sm-offset-0"><a class="btn" href="#applications" data-scroll>Anwendungen</a></li>

                <?php } elseif(ICL_LANGUAGE_CODE== 'fr') { ?>

                    <li class="col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1"><a class="btn" href="#products" data-scroll>Produits</a></li>
					<li class="col-md-5 col-md-offset-0 col-sm-5 col-sm-offset-0"><a class="btn" href="#applications" data-scroll>Applications</a></li>
                
                <?php } else { ?>
					<li class="col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1"><a class="btn" href="#products" data-scroll>Products</a></li>
					<li class="col-md-5 col-md-offset-0 col-sm-5 col-sm-offset-0"><a class="btn" href="#applications" data-scroll>Applications</a></li>
                <?php } ?>
				</ul>
			<?php
    } ?>
			<img src="<?php echo get_the_post_thumbnail_url(null, 'full'); ?>" alt="Image of <?php echo get_the_title(); ?>" />

		</div>
	</div>
	<div class="col-xs-12">
                    </div>
</div>
<?php
} ?>


<?php
$section_sub_title = get_field('section_sub_title');
$section_title = get_field('section_title');
$section_intro = get_field('intro_text');
$hide_section_intro = get_field('hide_intro_list');

if ($section_sub_title || $section_title) {
    echo '<div class="row"><div class="col-xs-12">';
}
if ($section_title) {
    echo '<h1 class="product-page-title">'.$section_title.'</h1>';
}
if ($section_intro) {
    if ($hide_section_intro) {
        //the hide button was pressed
    } else {
        echo '<div class="product-page-sub" style="text-align:left;"><span class="dark-intro">'.$section_intro.'</span></div>';
    }
} else {
    if ($section_title) {
        echo '<p class="product-page-sub">'.$section_sub_title.'';
    }
}
if ($section_sub_title || $section_title) {
    echo '</div></div>';
}
?>
