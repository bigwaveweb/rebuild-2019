<?php
$testimonial = get_posts(array(
  'post_type' => 'products',
  'meta_query' => array(
    array(
      'key' => 'testimonial_product',
      'value' => '"' . get_the_ID() . '"', // matches exactly "123", not just 123. This prevents a match for "1234"
			'compare' => 'LIKE'
    )
  )
));
?>

<?php if($testimonials) {
  foreach($testimonials as $testimonial) {
    $video = get_field('video', $testimonial->ID);
  }
  print_r($video);
}
