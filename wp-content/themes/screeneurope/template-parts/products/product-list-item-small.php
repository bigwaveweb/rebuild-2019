<li class="hidden-xs">
	<a href="<?php echo get_permalink();?>" data-productidx="<?php echo get_the_ID();?>">
		<img src="<?php echo get_the_post_thumbnail_url( null, 'full'); ?>" alt="Image of <?php echo get_the_title();?>" />
		<span><?php echo get_the_title();?></span>
	</a>
</li>
