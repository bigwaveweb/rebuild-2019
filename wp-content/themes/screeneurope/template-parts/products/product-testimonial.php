<?php
if( isset($_REQUEST['product_ids']) || isset($_POST['product_ids']) ){

	$prods = isset($_POST['product_ids']) ? $_POST['product_ids'] : NULL;
	if(!empty($_REQUEST['product_ids'])){
		$prods = $_REQUEST['product_ids'];
	}
	$testimonials =  products_solutions::getParentProductTestimonials( $prods, true, 10 );
	if( $testimonials  && !empty($testimonials)){
	?>
	<div class="col-xs-12 product-testimonial">
		<?php if(!isset($_REQUEST['no_title'])){ 
				if( ICL_LANGUAGE_CODE == 'de') {?>
					<h3 class="text-center">Aussagen unserer Kunden</h3>
				<?php } elseif( ICL_LANGUAGE_CODE == 'fr') {?> 
					<h3 class="text-center">Témoignages de nos clients</h3>
				<?php } else { ?>
					<h3 class="text-center">What Our Customers Say</h3>
		<?php } } ?>
		<div class="row">
			<div class="col-xs-12">
				<div id="quotes">
					<div class="carousel-inner testimonial-slider" role="listbox">
						<?php foreach( $testimonials as $item ) {
							echo sgt::non_translate(products_solutions::getTestimonialHtml($item));
						}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php }
}
?>
