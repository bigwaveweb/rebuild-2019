<!-- Footer Product Links -->
<div class="container">
	<div class="row">
		<?php build_footer_list('high-speed-inkjet');?>
		<?php build_footer_list('label-packaging');?>
		<?php build_footer_list('wide-format');?>
		<?php build_footer_list('prepress');?>
	</div>
</div>
<!-- /Footer Product Links -->
