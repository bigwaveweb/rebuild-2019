<nav id="site-navigation" class="navbar bg-white js-shrink-nav-on-scroll" role="navigation">
	<div class="container-fluid container-lg">

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
			        data-toggle="collapse"
			        data-target="#top-menu-navbar-collapse"
			        aria-expanded="false"
			>
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<div class="lang_switch_mob"><?php do_action('wpml_add_language_selector'); ?></div>
			<a class="navbar-brand" href="/">
				<div class="logo__holder">
					<?php
                    $custom_logo_id = get_theme_mod('custom_logo');
                    $logo = wp_get_attachment_image_src($custom_logo_id, 'full');
                    $logo_url = reset($logo);
                    $blog_name = get_bloginfo('name');
                    $description = get_bloginfo('description', 'display');

                    if (has_custom_logo()) {
                        echo "<img class=\"logo__custom\" src=\"{$logo_url}\" alt=\"{$blog_name}\">";
                    } else {
                        echo "<h1>{$blog_name}</h1>";
                    }

                    if ($description || is_customize_preview()) {
                        echo "<p class=\"site-description hidden-sm hidden-xs\">{$description}</p>";
                    }
                    ?>
				</div>
			</a>

		</div>

		<div id="top-menu-navbar-collapse" class="navbar-collapse collapse">
			
			<a class="nav-home hidden visible-xs" href="/">
				<i class="fa fa-home"></i> Home
			</a>
			<div class="tbcontainer">
				<div class="container" style="position: relative;">
					<div class="aligner">
						<?php wp_nav_menu([
                            'theme_location' => 'top',
                            'menu_id' => 'top-menu',
                            'depth' => 2,
                            'container' => null,
                            'menu_class' => 'nav navbar-nav',
                            'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                            'walker' => new WP_Bootstrap_Navwalker(),
                        ]); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="lang_switch"><?php do_action('wpml_add_language_selector'); ?></div>
		<div class="search__container">
			<div class="search__wrapper">
				<a href="#">
					<i class="fa fa-search" style="display:block"></i>
					<i class="fa fa-times" aria-hidden="true" style="display:none" ></i>
				</a>
			</div>
		</div>
	</div>
</nav>
