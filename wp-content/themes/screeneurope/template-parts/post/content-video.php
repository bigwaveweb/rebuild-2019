<?php
/**
 * Template part for displaying video posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Screen Europe
 * @since 1.0
 * @version 1.2
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="video__container">
		<?php
			if ( is_sticky() && is_home() ) {
				echo screeneurope_get_svg( array( 'icon' => 'thumb-tack' ) );
			}
		?>
		<header class="entry-header">
			<?php if ( 'post' === get_post_type() ) : ?>
				<div class="entry-meta">
					<?php echo get_the_date(); ?>
				</div><!-- .entry-meta -->
			<?php endif;

				if ( is_single() ) {
					the_title( '<h1 class="entry-title">', '</h1>' );
				} elseif ( is_front_page() && is_home() ) {
					the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
				} else {
					the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
				}
			?>
		</header><!-- .entry-header -->

		<?php
			$content = apply_filters( 'the_content', get_the_content() );
			$video = false;

			// Only get video from the content if a playlist isn't present.
			if ( false === strpos( $content, 'wp-playlist-script' ) ) {
				$video = get_media_embedded_in_content( $content, array( 'video', 'object', 'embed', 'iframe' ) );
			}
		?>

		<?php if ( '' !== get_the_post_thumbnail() && ! is_single() && empty( $video ) ) : ?>
			<div class="post-thumbnail">
				<a href="<?php the_permalink(); ?>">
					<?php the_post_thumbnail( 'screeneurope-featured-image' ); ?>
				</a>
			</div><!-- .post-thumbnail -->
		<?php endif; ?>

		<div class="entry-content">

			<?php
			if ( ! is_single() ) {

				// If not a single post, highlight the video file.
				if ( ! empty( $video ) ) {
					foreach ( $video as $video_html ) {
						echo '<div class="entry-video">';
							echo $video_html;
						echo '</div>';
					}
				}; ?>
				<?php the_excerpt(); ?>
			<?php };

			if ( is_single() || empty( $video ) ) {

				/* translators: %s: Name of current post */
				the_content( sprintf(
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'screeneurope' ),
					get_the_title()
				) );

				wp_link_pages( array(
					'before'      => '<div class="page-links">' . __( 'Pages:', 'screeneurope' ),
					'after'       => '</div>',
					'link_before' => '<span class="page-number">',
					'link_after'  => '</span>',
				) );
			};
			?>

		</div><!-- .entry-content -->

		<?php
		if ( is_single() ) {
			screeneurope_entry_footer();
		}
		?>
	</div>
	<div class="tags__container">
		<?php echo get_the_tag_list('<div><span class="label label-default">','</span> <span class="label label-default">','</span></div>'); ?>
	</div>
</article><!-- #post-## -->

<script>
var head = jQuery("iframe").contents().find("head");
var css = '<style type="text/css">' +
          '.ytp-cued-thumbnail-overlay-image{border-radius: 50%;}; ' +
          '</style>';
jQuery(head).append(css);
</script>
