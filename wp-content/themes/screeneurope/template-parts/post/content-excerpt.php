<?php
/**
 * Template part for displaying posts with excerpts
 *
 * Used in Search Results and for Recent Posts in Front Page panels.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Screen_Europe
 * @since 1.0
 * @version 1.2
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php if ( 'post' === get_post_type() ) : ?>
			<div class="entry-meta">
				<?php echo get_the_date(); ?>
			</div><!-- .entry-meta -->
		<?php endif; ?>

		<?php if ( is_front_page() && ! is_home() ) {

			// The excerpt is being displayed within a front page section, so it's a lower hierarchy than h2.
			the_title( sprintf( '<h3 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' );
		} else {
			the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
		} ?>
	</header><!-- .entry-header -->
	<?php	// check if the post has a Post Thumbnail assigned to it.
		if ( has_post_thumbnail() ) : ?>
			<div class="news-image__container">
				<?php the_post_thumbnail(); ?>
			</div>
	<?php endif; ?>
	<div class="entry-summary">
		<?php
		echo sgt::non_translate(preg_replace( "/<a href=.*?>(.*?)<\/a>/", "", get_the_excerpt() ));
		?>
		<div class="tags__container">
			<?php echo get_the_tag_list('<div><span class="label label-default">','</span> <span class="label label-default">','</span></div>'); ?>
		</div>
		<div class="text-right read-more">
			<a class="btn btn-primary btn--read-more" href="<?php echo esc_url( get_permalink() ); ?>">Read More&nbsp;»</a>
		</div>
	</div>

</article><!-- #post-## -->
