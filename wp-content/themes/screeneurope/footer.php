			</div>
			<footer id="colophon" class="site-footer" role="contentinfo">
				<div id="footer1" class="site-footer-section bg-brand-primary-darkest hidden-xs">
					<div class="container">
						<div class="row">
							<div class="col-xs-6">
								<a href="/">
									<img src="/wp-content/themes/screeneurope/assets/images/logo-reverse.svg" alt="Screen Europe Logo">
								</a>
							</div>
							<div class="col-xs-6 text-right">
								<a href="https://www.youtube.com/user/ScreenUSAtv" target="_blank" class="icon icon-circle bg-white">
									<i class="fa fa-youtube-play"></i>
								</a>
								<a href="https://uk.linkedin.com/company/dainippon-screen-europe" target="_blank" class="icon icon-circle bg-white">
									<i class="fa fa-linkedin"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div id="footer2" class="site-footer-section footer-menu-top bg-brand-primary-darker hidden-xs">
					<?php get_template_part( 'template-parts/products/footer-links' );?>
				</div>
				<div id="footer3" class="site-footer-section footer-menu-bottom bg-blue-dark">
					<div class="container">
						<div class="row">
							<div class="col-xs-6 small-screens--hide">
								<?php wp_nav_menu( array( 'theme_location' => 'footer-bottom' ) ); ?>
							</div>
							<?php if ( is_active_sidebar( 'footer-widget-1' ) ) : ?>
							<div class="col-xs-3 small-screens--hide">
								<div class="contact__container">
									<div id="sidebar-contact" class="sidebar widget-area" role="complementary">
										<?php dynamic_sidebar('footer-widget-1'); ?>
									</div>
								</div>
							</div>
							<?php endif; ?>
							<?php if ( is_active_sidebar( 'footer-widget-2' ) ) : ?>
							<div class="col-xs-3 small-screens--hide">
								<div class="copyright__container">
									<div id="sidebar-copyright" class="sidebar widget-area" role="complementary">
										<?php dynamic_sidebar('footer-widget-2'); ?>
									</div>
								</div>
								<?php /*
								<div style="position:relative" class="translate-container">
									<?php echo sgt::code();?>
								</div>
								*/ ?>
							</div>
							<?php endif; ?>
							<?php if ( is_active_sidebar( 'footer-widget-3' ) ) : ?>
							<div class="visible-xs col-sm-12">
								<div class="copyright__container copyright__container--mobile text-center">
									<div id="sidebar-copyright" class="sidebar widget-area" role="complementary">
										<?php dynamic_sidebar('footer-widget-3'); ?>
									</div>
								</div>
							</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<div class="violator violator-right js-back-to-top">
					<i class="fa fa-chevron-up"></i>
				</div>
			</footer>
		</div>
	</div>
	<script src="https://use.typekit.net/rmh8xws.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.js"></script>
	<script src="/wp-content/themes/screeneurope/assets/js/scripts.js"></script>
	<?php wp_footer();?>
</body>
