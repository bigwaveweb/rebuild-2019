<?php
/**
 * Screen Europe functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Screen_Europe
 * @since 1.0
 */

/**
 * Screen Europe only works in WordPress 4.7 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.7-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
	return;
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function screeneurope_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/screeneurope
	 * If you're building a theme based on Screen Europe, use a find and replace
	 * to change 'screeneurope' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'screeneurope' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'top'    => __( 'Top Menu', 'screeneurope' ),
		'footer-bottom' => __('Footer - bottom', 'screeneurope')
	) );

	// Add Bootstrap Nav Walker
	require_once('vendor/wp-bootstrap-navwalker/wp-bootstrap-navwalker.php');

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'audio',
	) );

	// Add theme support for Custom Logo.
	add_theme_support( 'custom-logo', array(
		'width'       => 300,
		'height'      => 40,
		'flex-width'  => true,
		'flex-height' => true,
		'header-selector' => false,
		'header-text'     => array( 'site-title', 'site-description' ),
	) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

}
add_action( 'after_setup_theme', 'screeneurope_setup' );

/**
 * Add support for SVG media
 *
 * @param $mimes
 *
 * @return mixed
 */
function cc_mime_types( $mimes ) {

	$mimes['svg'] = 'image/svg+xml';

	return $mimes;
}
add_filter( 'upload_mimes', 'cc_mime_types' );

/**
 * Convert flat tags to hierarchical
 * allowing them to be grouped
 */
function wd_hierarchical_tags_register() {

  // Maintain the built-in rewrite functionality of WordPress tags

  global $wp_rewrite;

  $rewrite =  array(
    'hierarchical'              => false, // Maintains tag permalink structure
    'slug'                      => get_option('tag_base') ? get_option('tag_base') : 'tag',
    'with_front'                => ! get_option('tag_base') || $wp_rewrite->using_index_permalinks(),
    'ep_mask'                   => EP_TAGS,
  );

  // Redefine tag labels (or leave them the same)

  $labels = array(
    'name'                       => _x( 'Tags', 'Taxonomy General Name', 'hierarchical_tags' ),
    'singular_name'              => _x( 'Tag', 'Taxonomy Singular Name', 'hierarchical_tags' ),
    'menu_name'                  => __( 'Taxonomy', 'hierarchical_tags' ),
    'all_items'                  => __( 'All Tags', 'hierarchical_tags' ),
    'parent_item'                => __( 'Parent Tag', 'hierarchical_tags' ),
    'parent_item_colon'          => __( 'Parent Tag:', 'hierarchical_tags' ),
    'new_item_name'              => __( 'New Tag Name', 'hierarchical_tags' ),
    'add_new_item'               => __( 'Add New Tag', 'hierarchical_tags' ),
    'edit_item'                  => __( 'Edit Tag', 'hierarchical_tags' ),
    'update_item'                => __( 'Update Tag', 'hierarchical_tags' ),
    'view_item'                  => __( 'View Tag', 'hierarchical_tags' ),
    'separate_items_with_commas' => __( 'Separate tags with commas', 'hierarchical_tags' ),
    'add_or_remove_items'        => __( 'Add or remove tags', 'hierarchical_tags' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'hierarchical_tags' ),
    'popular_items'              => __( 'Popular Tags', 'hierarchical_tags' ),
    'search_items'               => __( 'Search Tags', 'hierarchical_tags' ),
    'not_found'                  => __( 'Not Found', 'hierarchical_tags' ),
  );

  // Override structure of built-in WordPress tags

  register_taxonomy( 'post_tag', 'post', array(
    'hierarchical'              => true, // Was false, now set to true
    'query_var'                 => 'tag',
    'labels'                    => $labels,
    'rewrite'                   => $rewrite,
    'public'                    => true,
    'show_ui'                   => true,
    'show_admin_column'         => true,
    '_builtin'                  => true,
  ) );

}
add_action('init', 'wd_hierarchical_tags_register');

function show_all_tags_by_parent() {
	// get all tags from current item
	$tags 	= get_tags(array('hide_empty'=>false, 'orderby'=> 'asc'));
	$parents 	= get_tags(array('hide_empty'=>false, 'parent'=>0));

	// loop through parents and echo children grouped by parent
	foreach ($parents as $key => $parent ) {

		echo '<h3 class="tag__parent">' . $parent->name . '</h3>';

		foreach ($tags as $key => $tag) :
			$tag_link = get_tag_link( $tag->term_id );

			if ($tag->parent == $parent->term_id) :
				if ($tag->parent !== 0) :
					echo "<span class=\"label label-default\"><a href=\"{$tag_link}\" class=\"{$tag->slug}\">{$tag->name}</a></span> ";
				endif;
			endif;
		endforeach;
	}
}
function get_first_video_embed($post_id) {
	$content = apply_filters('the_content', get_post_field('post_content', $post_id));
	$iframes = get_media_embedded_in_content( $content, 'iframe' );
	return $video_post_iframe = $iframes[0];
}

function truncate_string($string,$length=400,$append=" &hellip;") {
  $string = trim($string);

  if(strlen($string) > $length) {
    $string = wordwrap($string, $length);
    $string = explode("\n", $string, 2);
    $string = $string[0] . $append;
  }

  return $string;
}

/**
 * Register widget areas.
 *
 */
function screeneurope_widgets_init() {
	// TO DO - sidebar for news/media posts
	register_sidebar( array(
		'name'          => __( 'News Topics Sidebar', 'screeneurope' ),
		'id'            => 'sidebar-news-1',
		'description'   => __( 'Additional sidebar that appears on the right.', 'twentyfourteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'News Single Post Sidebar', 'screeneurope' ),
		'id'            => 'sidebar-news-single',
		'description'   => __( 'Additional sidebar position that displays on the right of individual news posts.', 'twentyfourteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title title-underline">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'News Single Post Sidebar (bottom)', 'screeneurope' ),
		'id'            => 'sidebar-news-single-bottom',
		'description'   => __( 'Additional sidebar position that displays on the right of individual news posts.', 'twentyfourteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s recent-posts">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title title-underline">',
		'after_title'   => '</h3>',
	) );
	// footer widgets
	register_sidebar( array(
		'name'          => __( 'Footer Widget Area 1', 'screeneurope' ),
		'id'            => 'footer-widget-1',
		'description'   => __( 'Appears in the footer contact section of the site.', 'screeneurope' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Widget Area 2', 'screeneurope' ),
		'id'            => 'footer-widget-2',
		'description'   => __( 'Appears in the footer copyright section of the site.', 'screeneurope' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Widget Area 3 (MOBILE)', 'screeneurope' ),
		'id'            => 'footer-widget-3',
		'description'   => __( 'MOBILE only - appears in the footer copyright section of the site.', 'screeneurope' ),
		'before_widget' => '<aside id="%1$s" class="visible-xs widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
}
add_action( 'widgets_init', 'screeneurope_widgets_init' );

// read more show even with excerpt text
function new_excerpt_more($more) {
   global $post;
   return '... <a href="'. get_permalink($post->ID) . '">' . 'Read More &raquo;' . '</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');



/**
 * Implement the Custom Header feature.
 */
require get_parent_theme_file_path( '/inc/custom-header.php' );

/**
 * Custom template tags for this theme.
 */
require get_parent_theme_file_path( '/inc/template-tags.php' );

/**
 * Additional features to allow styling of the templates.
 */
require get_parent_theme_file_path( '/inc/template-functions.php' );

/**
 * Customizer additions.
 */
require get_parent_theme_file_path( '/inc/customizer.php' );

/**
 * SVG icons functions and filters.
 */
require get_parent_theme_file_path( '/inc/icon-functions.php' );

/**
 * Get Video Posts Via Ajax
 */
require get_parent_theme_file_path( '/inc/template-video.php' );

/**
 * Solutions / Products
 */
require get_parent_theme_file_path( '/inc/products-solutions.php' );

/**
 * Add some search
 */
require get_parent_theme_file_path( '/inc/search.php' );

/**
 * Google Translate code
 */
require get_parent_theme_file_path( '/inc/google-translate.php' );


remove_action( 'shutdown', 'wp_ob_end_flush_all', 1 );

