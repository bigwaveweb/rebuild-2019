<?php
/*
Template Name: POD Page
*/
get_header(); ?>

<style>
    
.form input {
width: 100%;
padding: 5px;
border: solid 1px #dddddd;
border-radius: 2px;
}
    
</style>

<div class="wrap">
	<div class="container">
			<div id="primary" class="content-area">
				<main id="main" class="site-main bg-white" role="main">
          <div class="row">
					<div class="col-md-12 pod-event">
						<h3>SCREEN & PRINTONDEMAND-WORLDWIDE TAKE INKJET PRINTING TO THE NEXT LEVEL WITH LITHO QUALITY ON LITHO STOCK</h3>
            <p><strong>Date:</strong> Wednesday 6th June 2018</p>
            <p><strong>Time:</strong> 10.15 - 15.00</p>
            <p><strong>Location:</strong> Marriott Hotel, Peterborough, PE2 6GB</p>
            <a href="#form" class="btn btn-primary">Register Here</a>
          </div>
        </div>
          <div class="row">
            <div class="col-md-6 form">
              <p>On Wednesday 6th June SCREEN Europe in association with Printondemand-worldwide will be holding an open day in Peterborough. During the event you will be able to see for yourself the Truepress Jet520HD in action at Printondemand-Worldwide. This is the world's first inkjet printer using specially developed SC inks to print on litho paper, thus removing the need for ink-jet coated papers.</p>
              <p>Also joining us at this event will be representatives from Hunkeler and Muller Martini whose finishing solutions combined with the Screen Truepress Jet 520HD using our unique SC inks, have made the efficient digital production of short-run books a reality.</p>
              <p>Andy Cork, Owner and Managing director of Printondemand-Worldwide will welcome you to the event at the Marriott Hotel in Peterborough where Screen, together with our partners, will give a brief overview of the equipment installed to deliver this 'Book of One' solution.</p>
              <p>Following a buffet lunch at the hotel you will be taken to the nearby Printondemand-Worldwide facility for a tour of the factory and to see the book production line full swing.</p>
              <h3>Itinerary</h3>
              <table class="table table-hover table-striped table-bordered">
              <thead>
                <tr>
                  <td class="tg-yw4l"><strong>Schedule</strong></td>
                  <td class="tg-yw4l"><strong>Location</strong></td>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="tg-yw4l">10.15 - 10.45 Arrival and Coffee</td>
                  <td class="tg-yw4l tg-centre" rowspan="6">Peterborough Marriott Hotel</td>
                </tr>
                <tr>
                  <td class="tg-yw4l">10.45 - 11.00  Introduction by Mr. Andy Cork and Mr. Bui Burke</td>
                </tr>
                <tr>
                  <td class="tg-yw4l">11.00 - 11.30  Inkjet Printing on Offset Coated Paper (Mr. Cees Rem)</td>
                </tr>
                <tr>
                  <td class="tg-yw4l">11.30 - 12.00  In-line finishing solutions (Mr. Robin Brown)</td>
                </tr>
                <tr>
                  <td class="tg-yw4l">11.00 - 12.45  The art of bookbinding (Mr. Ian Clark)</td>
                </tr>
                <tr>
                  <td class="tg-yw4l">12.45 - 1.30  Buffet Lunch</td>
                </tr>
                <tr>
                  <td colspan="2" class="tg-yw4l">1.45  Coach transfer to Printondemand Worldwide book factory</td>
                </tr>
                <tr>
                  <td class="tg-yw4l">2.00 - 3.00  Factory Tour / Live Book Production Experience<br /><br />On arrival at Printondemand-Worldwide we will split into 3 groups. After a short tour, we will focus in detail on the complete inline configuration of Screen Truepress Jet520HD with Hunkeler roll to book-block configuration and the Muller Martini binding solution.</td>
                  <td class="tg-yw4l tg-centre" rowspan="2">Printondemand Worldwide Peterborough</td>
                </tr>
                <tr>
                  <td class="tg-yw4l">3.00 Coach transfer back to hotel</td>
                </tr>
                <tr>
                  <td class="tg-yw4l" id="form">3:15 Networking Opportunity</td>
                  <td class="tg-yw4l tg-centre">Peterborough Marriott Hotel</td>
                </tr>
              </tbody>
            </table>

              <h3>Registration Form</h3>
              <?php echo do_shortcode('[contact-form-7 id="2002" title="POD Registration"]'); ?>
              <p>If you have any questions about the event please do not hesitate to contact us at <a href="mailto:marketing@screeneurope.com">marketing@screeneurope.com</a> or speak with your Sales Representative.</p>
            </div>

            <div class="col-md-6 pod-images">
              <img src="https://www.screeneurope.com/wp-content/uploads/2018/04/Screen-Print-On-Demand.00_00_08_20.Still003.jpg" />
              <img src="https://www.screeneurope.com/wp-content/uploads/2018/04/Screen-Print-On-Demand.00_00_59_14.Still006.jpg" />
              <img src="https://www.screeneurope.com/wp-content/uploads/2018/04/Screen-Print-On-Demand.00_00_23_15.Still004.jpg" />
              <img src="https://www.screeneurope.com/wp-content/uploads/2018/04/Screen-Print-On-Demand.00_04_01_11.Still002.jpg" />
           </div>
         </div>
         <div class="row">
          <div class="col-md-12 partnership">
            <h3>In Partnership With</h3>
            <div class="col-md-4">
              <img src="https://www.screeneurope.com/wp-content/uploads/2018/04/PrintOnDemand-Logo.jpg" />
            </div>
            <div class="col-md-4">
              <img src="https://www.screeneurope.com/wp-content/uploads/2018/04/Logo_maller-martini.jpg" />

            </div>
            <div class="col-md-4">
              <img src="https://www.screeneurope.com/wp-content/uploads/2018/04/Hunkeler_CMYK.jpg" />
            </div>
          </div>
        </div>
				</main>
			</div>
	</div>
</div>

<script>
  jQuery(document).ready(function($) {
    // Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
      &&
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });
  });
</script>
<?php get_footer();
