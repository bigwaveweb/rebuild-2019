<?php
get_header();
$glob_id = null;
?>
<div class="container">
	<div id="primary" class="content-area">
		<main id="main" role="main">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<?php
					echo '<div class="trust-content">';
						while ( have_posts() ) : the_post();
						$glob_id = get_the_ID();
						echo sgt::non_translate(get_the_content());
						endwhile;
					echo '</div>';
					?>
				</div>
				<div class="col-xs-12 col-sm-6">
					<div class="trust-video">
						<?php
						$video =  get_field( "trust_video", $glob_id);
						echo $video;
						?>
					</div>
				</div>
				<?php
				$rows = get_field('trust_trust_item', $glob_id);
				if($rows)
				{
					echo '
					<div class="col-xs-12">
						<div class="row">
					';
					foreach($rows as $row){
						echo '<div class="col-xs-12 col-sm-4 trust-item">';
							echo '
							<h3>'.$row['title'].'</h3>
							<img class="trust-title-img" src="'.$row['subtitle']['url'].'" alt="Image of '. $row['title'].'" />
							<img class="trust-section-img" src="'.$row['image']['url'].'" alt="Image of '.$row['title'].'" />
							'.sgt::non_translate($row['text']).'
							';
						echo '</div>';
					}
					echo '
						</div>
					</div>
					';
				}
		echo '
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="support_news_items">
					<div class="row">
			';
						$news_items = products_solutions::getFourTaggedSolutions();
						if ( !empty($news_items) ) {
							foreach($news_items as $news ){
								$formatVideo = products_solutions::isVideoFormat( $news->ID );
								if(!$formatVideo){ ?>
									<div class="col-xs-12 col-sm-6 story">
										<h3 class="light">
											<a href="<?php echo get_the_permalink($news->ID);?>"><?php echo get_the_title($news->ID);?></a>
										</h3>
										<div class="row">
											<div class="col-md-6">
												<div class="row">
													<div class="col-sm-11">
														<p>
															<a href="<?php echo get_the_permalink($news->ID);?>">
																<img src="<?php echo get_the_post_thumbnail_url($news->ID);?>" alt="Image from <?php echo get_the_title($news->ID);?>">
															</a>
														</p>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<p><?php echo truncate_string(strip_shortcodes(get_the_excerpt($news->post_excerpt)));?></p>
												<p><a href="<?php echo get_the_permalink($news->ID);?>" class="btn btn-default">Read More&nbsp;&raquo;</a></p>
											</div>
										</div>
									</div>
								<?php } else { ?>
									<div class="col-xs-12 col-sm-6 story">
										<h3 class="light"><?php echo sgt::non_translate(get_the_title($news->ID));?></h3>
										<div class="row">
											<div class="col-xs-12">
												<div class="embed-responsive embed-responsive-16by9">
													<?php
													$content = apply_filters( 'the_content', get_post_field('post_content', $news->ID ) );
													$video = false;
													if ( false === strpos( $content, 'wp-playlist-script' ) ) {
														$video = get_media_embedded_in_content( $content, array( 'video', 'object', 'embed', 'iframe' ) );
													}
													if(isset($video[0])){
														echo $video[0];
													}
													?>
												</div>
											</div>
												<?php
												$tags = get_the_tags($news->ID);
												if(!empty($tags)){
													echo '<div class="col-xs-12">';
														echo '<div class="tag-row">';
															foreach($tags as $tag){
																echo '<span class="label label-muted">'.$tag->name.'</span> ';
															}
													echo '</div>
													</div>';
												}?>
										</div>
										<p><?php echo truncate_string( strip_shortcodes( $news->post_excerpt ));?></p>
									</div>
								<?php
								}
							}
						}
						?>
						</div>
					</div>
				</div>
			</div>
		</main>
	</div>
</div>
<?php get_footer();
