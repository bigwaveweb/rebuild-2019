<?php
get_header(); ?>
<div class="wrap" id="archive">
	<div class="container">
		<div class="row">
			<?php if ( have_posts() ) : ?>
				<header class="page-header">
					<?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );
					the_archive_description( '<div class="taxonomy-description">', '</div>' );
					?>
				</header>
			<?php endif; ?>
			<div id="primary" class="content-area">
				<main id="main" class="site-main" role="main">
				<?php
				if ( have_posts() ) : ?>
					<?php
					while ( have_posts() ) : the_post();
						get_template_part('template-parts/post/content-excerpt', get_post_format());
					endwhile;
					the_posts_pagination( array(
						'prev_text' => '<span class="screen-reader-text">' . __( 'Previous', 'screeneurope' ) . '</span>',
						'next_text' => '<span class="screen-reader-text">' . __( 'Next', 'screeneurope' ) . '</span>',
						'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( false, 'screeneurope' ) . ' </span>',
					) ); ?>
				<?php else :
					get_template_part( 'template-parts/post/content', 'none' );
				endif; ?>
				</main>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
</div>
<?php get_footer();
