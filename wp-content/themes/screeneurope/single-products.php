<?php
get_header();
$glob_id = get_the_ID();

$language_translation_links = array();

$language_translation_links['en'] = array(
	'Features'			=> 'Features',
	'Applications'		=> 'Applications',
	'Success Stories'	=> 'Success Stories',
	'Literature'		=> 'Literature',
	'Specifications'	=> 'Specifications',
	'Testimonials'		=> 'Testimonials',
);

$language_translation_links['de'] = array(
	'Features'			=> 'Funktionen',
	'Applications'		=> 'Anwendungen',
	'Success Stories'	=> 'Erfolgsgeschichten',
	'Literature'		=> 'Literatur',
	'Specifications'	=> 'Spezifikationen',
	'Testimonials'		=> 'Referenzen',
);

$language_translation_links['fr'] = array(
	'Features'			=> 'Caractéristiques',
	'Applications'		=> 'Applications',
	'Success Stories'	=> 'Histoires de réussite',
	'Literature'		=> 'Documentation',
	'Specifications'	=> 'Spécifications',
	'Testimonials'		=> 'Témoignages',
);

if ( defined( 'ICL_LANGUAGE_CODE' ) ) {
  echo '****' . ICL_LANGUAGE_CODE;
}

/**
 * Are there any specifications
 * --.
 *
 * @param [type] $glob_id [description]
 *
 * @return bool [description]
 */
function hasSpecifications($glob_id = null)
{
    $image = get_field('spec_image', $glob_id);
    $image2 = get_field('spec_image_mobile', $glob_id);
    $haveTable = false;
    $dimensions = get_field('spec_dimensions', $glob_id);
    if (isset($dimensions['body']) && !empty($dimensions['body'])) {
        $haveTable = true;
    }
    if ($haveTable || !empty($image) || have_rows('random', $glob_id)) {
        return true;
    }

    return false;
}
/**
 * Are there any applications
 * --.
 *
 * @param [type] $glob_id [description]
 *
 * @return bool [description]
 */
function hasApplications($glob_id = null)
{
    $applications = get_field('applications', $glob_id);
    if (!empty($applications)) {
        return true;
    }

    return false;
}
/**
 * Is there any news
 * --.
 *
 * @return bool [description]
 */
function hasNews($glob_id = null)
{
    $news = get_related_stories(products_solutions::get_end_slug());
    if ($news->have_posts()) {
        return true;
    }

    return false;
}
/**
 * Is there a brochure ?
 * --.
 *
 * @param [type] $glob_id [description]
 *
 * @return bool [description]
 */
function hasLit($glob_id = null)
{
    $brochure = get_field('brochure', $glob_id);
    if ($brochure && !empty($brochure)) {
        return true;
    }

    return false;
}

echo '
<div id="subnavigation" class="collapse" role="menu">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="reverse">
					<h2>'.get_the_title().'&nbsp;<small>'.get_field('subtitle').'</small></h2>
					<a href="#features" data-scroll="">Features</a>
					';

                    if (hasApplications()) {
                        echo '<a href="#applications" data-scroll="">Applications</a>';
                    }

                    if (hasNews()) {
                        echo '<a href="#success-stories" data-scroll="">Success Stories</a>';
                    }

                    if (hasLit()) {
                        echo '<a href="#literature" data-scroll="">Literature</a>';
                    }

                    if (hasSpecifications()) {
                        echo '<a href="#specs" data-scroll="">Specifications</a>';
                    }

                    $product_customers = products_solutions::getProductCustomers($glob_id);
                    if ($product_customers && !empty($product_customers)) {
                        echo '<a href="#testimon" data-scroll="">Testimonials</a>';
                    }
                    echo'
				</div>
			</div>
		</div>
	</div>
</div>
';
?>
<section class="container">
	<div class="row">
		<div class="col-xs-12" >
			<main id="main" class="site-main" role="main">
				<?php
                if (have_posts()) :
                    while (have_posts()) : the_post();
                        $glob_id = get_the_ID(); ?>
						<div class="row">
							<div class="col-xs-12">
								<div class="product-banner-container">
									<?php
                                    $image = get_field('banner_image');
                                    if (!empty($image)) {
                                        echo '<img src="'.$image['url'].'" alt="'.$image['alt'].'" />';
                                    }
                                    ?>
								</div>
							</div>
						</div>
						<div class="row">
                        <div class="col-md-12">
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<h1><?php echo sgt::non_translate(get_the_title()); ?>&nbsp;<small><?php echo get_field('subtitle'); ?></small></h1>
							</div>
						</div>
						<div class="row" id="primary">
							<div class="col-xs-12">
								<div class="subnavigation" >
								<?php if (ICL_LANGUAGE_CODE == 'de') { ?>

									<a href="#features" data-scroll="">Leistungsmerkmale</a>
									<?php if (hasApplications($glob_id)) {
                                            ?>
										<a href="#applications" data-scroll="">Applications</a>
										<?php
                                        } ?>

										<?php if (hasNews($glob_id)) {
                                            ?>
										<a href="#success-stories" data-scroll="">Success Stories</a>
										<?php
                                        } ?>

										<?php if (hasLit($glob_id)) {
                                            ?>
										<a href="#literature" data-scroll="">Literature</a>
										<?php
                                        } ?>

										<?php if (hasSpecifications($glob_id)) {
                                            ?>
										<a href="#specs" data-scroll="">Specifications</a>
										<?php
                                        } ?>

										<?php
                                        if ($product_customers && !empty($product_customers)) {
                                            echo '<a href="#testimon" data-scroll="">Testimonials</a>';
                                        } ?>
									<?php } elseif (ICL_LANGUAGE_CODE == 'fr') { ?>
										<a href="#features" data-scroll="">Caractéristiques</a>
										<?php if (hasApplications($glob_id)) {
												?>
										<a href="#applications" data-scroll="">Applications</a>
										<?php
											} ?>

										<?php if (hasNews($glob_id)) {
												?>
										<a href="#success-stories" data-scroll="">Histoires de réussite</a>
										<?php
											} ?>

										<?php if (hasLit($glob_id)) {
												?>
										<a href="#literature" data-scroll="">Documentation</a>
										<?php
											} ?>

										<?php if (hasSpecifications($glob_id)) {
												?>
										<a href="#specs" data-scroll="">Spécifications</a>
										<?php } ?>

										<?php
										if ($product_customers && !empty($product_customers)) {
											echo '<a href="#testimon" data-scroll="">Témoignages</a>';
										} ?>
									<?php
                                    } else {
                                        ?>
									<a href="#features" data-scroll="">Features</a>

									<?php if (hasApplications($glob_id)) {
                                            ?>
									<a href="#applications" data-scroll="">Applications</a>
									<?php
                                        } ?>

									<?php if (hasNews($glob_id)) {
                                            ?>
									<a href="#success-stories" data-scroll="">Success Stories</a>
									<?php
                                        } ?>

									<?php if (hasLit($glob_id)) {
                                            ?>
									<a href="#literature" data-scroll="">Literature</a>
									<?php
                                        } ?>

									<?php if (hasSpecifications($glob_id)) {
                                            ?>
									<a href="#specs" data-scroll="">Specifications</a>
									<?php } ?>

									<?php
                                    if ($product_customers && !empty($product_customers)) {
                                        echo '<a href="#testimon" data-scroll="">Testimonials</a>';
                                    } ?>
								<?php }  ?>

								</div>
							</div>
						</div>
						<div id="features">
							<?php echo sgt::non_translate(get_the_content()); ?>
						</div>
					<?php
                    endwhile;
                    ?>
				<?php endif; ?>
			</main>
		</div>
	</div>
</section>
<?php $applications = get_field('applications', $glob_id);
if (!empty($applications)) {
    ?>
<section class="applications-container">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h2>Applications</h2>
				<div id="applications">
					<div class="row">
						<?php
                        $count = 0;
    foreach ($applications as $item) {
        ?>
							<div class="col-lg-3  col-sm-6 col-xs-12">
								<h3><?php echo $item->post_title; ?></h3>
								<?php $image_url = get_the_post_thumbnail_url($item->ID); ?>
								<p><img src="<?php echo $image_url; ?>" class="img-responsive" alt="Image of <?php echo $item->post_title; ?>"></p>
								<p><?php echo sgt::non_translate(products_solutions::subWords($item->post_content, 350)); ?></p>
							</div>
							<?php
                            ++$count;
        if ($count > 3) {
            $count = 0;
            echo '<div class="clearfix">&nbsp;</div>';
        } ?>
						<?php
    } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php
} ?>


<?php
$news = get_related_stories(products_solutions::get_end_slug());
if ($news->have_posts()) {
    ?>
<section class="stories-container">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h2>Success Stories</h2>
				<div id="success-stories" class="row">
					<?php

                    $limit = 3;
    $counter = 0;
    $limit = 0;
    $writeLink = false;

    if ($news->have_posts()) {
        while ($news->have_posts()) {
            if ($limit >= 4) {
                if (!$writeLink)
                {
                  if (ICL_LANGUAGE_CODE == 'de')
                  {
                    echo '<div class="col-xs-12" style="text-align:center">
                    <a href="/de/tag/'.products_solutions::get_end_slug().'" class="more-news btn btn-default">MEHR ERFOLGSGESCHICHTEN</a>
                    </div>';
                  }
                  elseif(ICL_LANGUAGE_CODE == 'fr')
                  {
                    echo '<div class="col-xs-12" style="text-align:center">
                    <a href="/fr/tag/'.products_solutions::get_end_slug().'" class="more-news btn btn-default">PLUS DE SUCCÈS</a>
                    </div>';
                  } else {
                    echo '<div class="col-xs-12" style="text-align:center">
                    <a href="/tag/'.products_solutions::get_end_slug().'" class="more-news btn btn-default">MORE SUCCESS STORIES</a>
                    </div>';
                  }
                }
                $writeLink = true;
                break;
            }

            $news->the_post();
            $formatVideo = get_post_format() === 'video';
            if (!$formatVideo) {
                ?>
								<div class="col-sm-6 story">
									<h3 class="light">
										<a href="<?php echo get_the_permalink(); ?>"><?php echo sgt::non_translate(products_solutions::subWords(get_the_title(), 80)); ?></a>
									</h3>
									<div class="row">
										<?php
                                        $img_src = get_the_post_thumbnail_url();
                if (!empty($img_src)) {
                    ?>
											<div class="col-md-6">
												<div class="row">
													<div class="col-sm-11">
														<p>
															<a href="<?php echo get_the_permalink(); ?>">
																<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="Image from <?php echo get_the_title(); ?>">
															</a>
														</p>
													</div>
												</div>
											</div>
											<div class="col-md-6">
											  <p><?php echo sgt::non_translate(truncate_string(strip_shortcodes(get_the_excerpt()))); ?></p>
												<p><a href="<?php echo get_the_permalink(); ?>" class="btn btn-default">Read More&nbsp;&raquo;</a></p>
											</div>
										<?php
                } else {
                    ?>
											<div class="col-xs-12">
											  <p><?php echo truncate_string(strip_shortcodes(get_the_excerpt())); ?></p>
												<p><a href="<?php echo get_the_permalink(); ?>" class="btn btn-default">Read More&nbsp;&raquo;</a></p>
											</div>
										<?php
                } ?>
									</div>
									<hr class="visible-xs">
								</div>
							<?php
            } else {
                ?>
								<div class="col-sm-6">
									<h3 class="light"><?php echo sgt::non_translate(products_solutions::subWords(get_the_title()), 80); ?></h3>
									<div class="row">
										<div class="col-xs-12">
											<div class="embed-responsive embed-responsive-16by9">
												<?php
                                                $content = apply_filters('the_content', get_the_content());
                $video = false;
                if (false === strpos($content, 'wp-playlist-script')) {
                    $video = get_media_embedded_in_content($content, array('video', 'object', 'embed', 'iframe'));
                }
                if (isset($video[0])) {
                    echo $video[0];
                } ?>
											</div>
										</div>
									</div>
									<p><?php echo sgt::non_translate(truncate_string(strip_shortcodes(get_the_excerpt()))); ?></p>
									<hr class="visible-xs"></div>
							<?php
            }
            if ($counter >= 1) {
                $counter = -1;
                echo '<div class="clearfix">&nbsp;</div>';
            }
            ++$counter;
            ++$limit;
        }
    } ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php
} ?>
<?php
$brochure = get_field('brochure', $glob_id);
if ($brochure && !empty($brochure)) {
    ?>
<section class="lit-container">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
			<?php if (ICL_LANGUAGE_CODE == 'de') { ?>
				<h2>Literature</h2>

			<?php } elseif (ICL_LANGUAGE_CODE == 'fr') { ?>
				<h2>Documentation</h2>

			<?php } else { ?>
				<h2>Literature</h2>

			<?php } ?>
				<div id="literature">
					<div class="row">
						<div class="col-xs-12 col-sm-4">
							<?php
                            $image = get_field('brochure_image', $glob_id);
							if (!empty($image)) {
								echo '<img src="'.$image['url'].'" alt="'.$image['alt'].'" />';
							} ?>
						</div>
						<div class="col-xs-12 col-sm-8">
							<?php get_template_part('template-parts/products/brochure-form'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php
} ?>

<?php
$image = get_field('spec_image', $glob_id);
$image2 = get_field('spec_image_mobile', $glob_id);

$haveTable = false;
//dimentions is the table
$dimensions = get_field('spec_dimensions', $glob_id);
if (isset($dimensions['body']) && !empty($dimensions['body'])) {
    //we have the table
    $haveTable = true;
}
if ($haveTable || !empty($image) || have_rows('random', $glob_id)) {
    ?>
<section class="specs-container">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h2>Specifications</h2>
				<?php if ($haveTable || !empty($image)) {
        ?>
					<div id="specs">
						<div class="row">
							<?php
                            echo '<div class="col-xs-12"><h3 class="dims-header">Dimensions</h3></div>';
        if ($haveTable) {
            if (!empty($image)) {
                echo '<div class="col-xs-12 col-lg-6">';
                echo '<div class="row">';

                echo '<div class="hidden-xs">';
                display_spec_images_from_gallery($image, $haveTable);
                echo '</div>';

                echo '<div class="visible-xs">';
                display_spec_images_from_gallery($image2, $haveTable);
                echo '</div>';

                echo '</div>';
                echo '</div>';
            }

            if (isset($dimensions) && !empty($dimensions)) {
                display_spec_tables($dimensions);
            }
        } else {
            if (!empty($image)) {
                echo '<div class="col-xs-12"><div class="specs-image-table"><div class="hidden-xs">'.$image.'</div></div></div>';
            }
            if (!empty($image2)) {
                echo '<div class="col-xs-12"><div class="specs-image-table"><div class="visible-xs">'.$image2.'</div></div></div>';
            }
        } ?>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12"></div>
						</div>
					<?php
    } ?>

					<?php if (have_rows('random', $glob_id)) {
        ?>
						<div class="row">
							<div class="col-xs-12">
								<table class="table table-stripped table-hover spec-table">
									<?php
                                    if (have_rows('random', $glob_id)):
                                        while (have_rows('random', $glob_id)) : the_row();
        $key = get_sub_field('key');
        $value = get_sub_field('value');
        if (!empty($value)) {
            echo '<tr><th>'.$key.'</th><td>'.sgt::non_translate($value).'</td></tr>';
        }
        endwhile;
        endif; ?>
								</table>
							</div>
							<?php $notes = get_field('spec_notes', $glob_id);
        if (!empty($notes)) {
            echo '<div class="col-xs-12 spec-notes">'.sgt::non_translate($notes).'</div>';
        } ?>
						</div>
					<?php
    } ?>

				</div>
			</div>
		</div>
	</div>
</section>
<?php
}
$product_customers = products_solutions::getProductCustomers($glob_id);
if ($product_customers) {
    ?>
<section class="testimonial-container">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<?php if(ICL_LANGUAGE_CODE== 'de') { ?>

					<h2>Einige Kunden von Screen, bei denen die <?php echo sgt::non_translate(get_the_title($glob_id)); ?> im Einsatz ist</h2>

				<?php } elseif (ICL_LANGUAGE_CODE== 'fr') { ?>

					<h2>Faites la connaissance de quelques <?php echo sgt::non_translate(get_the_title($glob_id)); ?> clients des systèmes</h2>

				<?php } else { ?>

					<h2>Meet Just a Few of Screen's <?php echo sgt::non_translate(get_the_title($glob_id)); ?> Customers</h2>


				<?php } ?>
				<div id="testimon">
					<div class="row">
						<?php
                        foreach ($product_customers as $customer) {
                            get_template_part('template-parts/products/product-customers-row');
                        } ?>
					</div>
				</div>
				<div class="video">
					<?php get_template_part('template-parts/products/video-testimonial'); ?>
				</div>
			</div>
			<div class="video">
				<?php get_template_part('template-parts/products/video-testimonial'); ?>
			</div>
		</div>
	</div>
</section>
<?php
} ?>
<?php get_footer(); ?>
