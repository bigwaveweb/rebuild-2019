<?php
get_header(); ?>
	<style>.site-content-contain{padding-top:31px !important;}</style>
	<div class="wrap">
		<div id="primary" class="content-area">
			<section id="hp-solutions" class="" role="feed">
				<div class="container__">
					<div class="widget-heading">
						<h4 class="f36">
							<?php if(ICL_LANGUAGE_CODE=='de') { ?>
								Unsere Lösungen
							<?php } elseif(ICL_LANGUAGE_CODE=='fr') { ?>
								NOS SOLUTIONS
							<?php } else { ?>
								Our Solutions
							<?php } ?>
						</h4>
						<hr>
					</div>
					<div class="container section section-less-margin section-first">
						<div class="row">
							<div class="col-lg-12 col-lg-offset-0 col-md-10 col-md-offset-1 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0">
								<div class="row">
								<?php foreach( products_solutions::getSolutions() as $item ) { $ID = $item->ID; ?>
								<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
									<div class="flip">
										<?php
										$image = get_field('homepage_image_big', $ID);
										$image_url = '/assets/img/flip-high-speed-inkjet.jpg';
										if( !empty($image) ){
											$image_url = $image['url'];
										}
										?>
										<a href="<?php echo get_the_permalink($ID);?>" class="front side" style="background-image:url('<?php echo $image_url;?>')">
											<div><h2><?php echo get_the_title($ID);?></h2></div>
										</a>
										<div class="back side">
											<h2>
												<a href="<?php echo get_the_permalink($ID);?>"><?php echo sgt::non_translate(get_the_title($ID));?></a>
											</h2>
											<hr>
											<p><?php echo get_field('section_sub_title', $ID);?></p>
											<div class="row">
												<div class="col-xs-10 col-xs-offset-1 text-center">
													<p>
														<a href="<?php echo get_the_permalink($ID);?>">
															<?php
															$image = get_field('homepage_image_small', $ID);
															if( !empty($image) ){ ?>
																<img src="<?php echo $image['url']; ?>" class="img-responsive" alt="<?php echo $image['alt']; ?>" />
															<?php } ?>
														</p>
													</div>
												</div>
												<p><a href="<?php echo get_the_permalink($ID);?>" class="btn btn-default">

												<?php if(ICL_LANGUAGE_CODE=='de') { ?>
													DISCOVER MORE
												<?php } elseif(ICL_LANGUAGE_CODE=='fr') { ?>
													EN SAVOIR PLUS
												<?php } else { ?>
													DISCOVER MORE
												<?php } ?>
												</a></p>
										</div>
									</div>
								</div>
								<?php } ?>
						</div>
					</div>
				</div>
			</section>
			<section id="newsfeed" class="widget-section bg-grey-lightest" role="feed">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="widget-heading">
								<h2 class="f36">Latest News &amp; Events</h2>
								<hr>
							</div>
						</div>
						<?php
							$getPosts = get_posts(
							array(
							'category' => 17,
							'numberposts' => 3,
							'orderby'	=> 'date'
							));
							$count = 0;
							foreach ($getPosts as $key => $post) :
								if($count > 2){
									continue;
								}
							?>
							<div class="col-md-4 col-sm-<?php echo $key === 2 ? '12' : '6'; ?>">
								<a class="news-title" href="<?php echo esc_url( get_the_permalink($post->ID) ); ?>"><h4><?php echo sgt::non_translate($post->post_title); ?></h4></a>
								<?php
								if ( has_post_thumbnail() ) : ?>
									<div class="news-image__container">
										<?php the_post_thumbnail(); ?>
									</div>
								<?php endif; ?>
								<div class="news-content">
									<?php
										$postFormat = get_post_format($post->ID);
										if ($postFormat == "video") :
											echo '<div class="responsive-iframe">' . get_first_video_embed($post->ID) . '</div>';
											if (!empty($post->post_excerpt)) :
												echo "<p>" . $post->post_excerpt . "</p>";
											else :
												echo '<p>'.sgt::non_translate(truncate_string(strip_shortcodes($post->post_content))) . '</p>';
											endif;

										else :
											if (!empty($post->post_excerpt)) :
												echo "<p>" . $post->post_excerpt . "</p>";
											else :
												echo '<p>'.sgt::non_translate(truncate_string(strip_shortcodes($post->post_content))) . '</p>';
											endif;
										endif;
									?>
								</div>
								<div class="news-read-more">
									<a class="btn--read-more" href="<?php echo esc_url( get_permalink() ); ?>">Read More&nbsp;»</a>
								</div>
								<div class="news-tags tags__container">
									<?php echo get_the_tag_list('<div><span class="label label-muted">','</span> <span class="label label-muted">','</span></div>'); ?>
								</div>
								<hr class="visible-xs">
							</div>
						<?php $count++; endforeach; ?>
					</div>
					<div class="row">
						<div class="col-md-12 text-center">
							<?php $news_category = get_category(17); ?>
							<a class="btn btn-primary btn--more-news" href="<?php echo get_category_link(17); ?>">MORE NEWS</a>
						</div>
					</div>
				</div>
			</section>
			<section id="quotes" class=" bg-white" role="feed">
				<div class="container">
					<div class="homepage-testimonials">
						<?php
						$_POST['product_ids'] = products_solutions::getHomepageTestimonialProducts();
						get_template_part( 'template-parts/products/product-testimonial' );
						unset($_POST['product_ids']);
						?>
					</div>
				</div>
			</section>
		</div>
	</div>
<?php get_footer();
