<?php
/*Plugin Name: Recent Posts by Category
* Description: Display recent posts by category.
* Version: 1.0
* Author: Jen Cant @ Bigwave Media
* Author URI: http://www.bigwavemedia.co.uk
* License: GPLv2
*/
class recent_posts_category extends WP_Widget {

	public function __construct() {
		parent::__construct(

			'recent_posts_category_widget',
			__('List Recent Posts by Category', 'screeneurope' ),
			// widget options
			array (
				'description' => __( 'Lists recent posts by category.', 'screeneurope' )
			)
		);
	}

	public function widget( $args, $instance ) {

		$title 	   = $instance['title'];
		// chosen category
		$category_id = $instance['cat'];
		// chosen post limit
		$post_count  = $instance['postcount'];

		$getPosts = get_posts(
				array(
					'category' => $category_id,
					'numberposts' => $post_count,
					'orderby'	=> 'date'
				)
			);

		?>

			<aside class="recent-posts recent-posts--category">
				<div class="recent-posts--post">
					<h3 class="widget-title title-underline"><?= $title; ?></h3>
				<?php
					$limit = 1;
					foreach ($getPosts as $key => $post) :
						if($limit > $post_count  ){
							continue;
						}
					?>

						<a href="<?php echo esc_url( get_the_permalink($post->ID) ); ?>"><h4><?php echo $post->post_title; ?></h4></a>
						<?php
							$postFormat = get_post_format($post->ID);

							if ($postFormat == "video") : // if its a video post, render the [embed] tag
								echo '<div class="responsive-iframe">' . get_first_video_embed($post->ID) . '</div>';

								if (!empty($post->post_excerpt)) :
									echo "<p>" . $post->post_excerpt . "</p>";
								else :
									echo '<p>'.truncate_string(strip_shortcodes($post->post_content)) . '</p>';
								endif;

							else : // if any other kind of post
								if (!empty($post->post_excerpt)) :
									echo "<p>" . $post->post_excerpt . "</p>";
								else :
									echo '<p>'.truncate_string(strip_shortcodes($post->post_content)) . '</p>';
								endif;
							endif;
						?>
						<div class="read-more">
							<a class="btn btn-primary btn--read-more" href="<?php echo esc_url( get_the_permalink($post->ID) ); ?>">Read More&nbsp;»</a>
						</div>
				<?php $limit ++; endforeach; ?>
				</div>
			</aside>

		<?php

	}

	public function form( $instance ) {

		$instance = wp_parse_args( (array) $instance, array( 'postcount' => '', 'title'=>'' ) );

		$title 	 = isset($instance['title']) ? $instance['title'] : '';
		$postcount = isset( $instance['postcount'] ) ? $instance['postcount'] : '';
		$cat 	 = $instance['cat'];

		$categories = get_categories();

		?>
			<p>
				<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Title:' ); ?></label>
				<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_attr( $title ); ?>" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'cat' ); ?>"><?php _e( 'Select Category:' ); ?></label>
				<select id="<?php echo $this->get_field_id( 'cat' ); ?>" name="<?php echo $this->get_field_name( 'cat' ); ?>" class="widefat">
					<option value="0"><?php _e( '&mdash; Select &mdash;' ); ?></option>
						<?php foreach ($categories as $category) : ?>
							<option value="<?= $category->cat_ID; ?>" <?php echo ($category->cat_ID == $cat )?'selected':''; ?>>
								<span><?= $category->name; ?></span>
							</option>
						<?php endforeach; ?>
				</select>
			</p>

			<p>
				<label for="<?php echo $this->get_field_id('postcount'); ?>"><?php _e( 'Number of posts to show:' ); ?></label>
				<input type="number" class="tiny-text" id="<?php echo $this->get_field_id( 'postcount' ); ?>" name="<?php echo $this->get_field_name( 'postcount' ); ?>" value="<?php echo esc_attr( $postcount ); ?>" step="1" min="1"/>
			</p>
		<?php //endif;

	}


	/**
	 * Handles updating settings for the current Custom Menu widget instance.
	 *
	 * @since 3.0.0
	 * @access public
	 *
	 * @param array $new_instance New settings for this instance as input by the user via WP_Widget::form().
	 * @param array $old_instance Old settings for this instance.
	 * @return array Updated settings to save.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance[ 'title' ] = sanitize_text_field($new_instance[ 'title' ]);
		$instance[ 'postcount' ] = sanitize_text_field($new_instance[ 'postcount' ]);
		$instance[ 'cat' ] = sanitize_text_field($new_instance[ 'cat' ]);

		return $instance;

	}


}
function register_recent_posts_category() {

	register_widget( 'recent_posts_category' );

}
add_action( 'widgets_init', 'register_recent_posts_category' );
