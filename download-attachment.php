<?php
/**
 * Download a file by name - used for the brochure downloads
 * --
 */
$filename = '';
if(isset($_REQUEST['f']) && !empty($_REQUEST['f'])){

	$filename = basename($_REQUEST['f']);
	$parts = explode('.', $filename );
	$extension = end($parts);
	$allowed = array('pdf');

	//make sure cant download and PHP files for example.
	if(!in_array($extension, $allowed)){
		die();
	}

}else{
	die();
}

header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename='.$filename);
readfile($_REQUEST['f']);
exit;
